<?php
/* Smarty version 3.1.33, created on 2020-02-24 15:16:03
  from 'C:\xampp\htdocs\pujcovna_kol\pujcovna_kol\templates\rezervaceUdaje.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e53daa372c103_64232683',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eef7a97510d534fa25a826e4fbd3f5b323b989c6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pujcovna_kol\\pujcovna_kol\\templates\\rezervaceUdaje.tpl',
      1 => 1582553547,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e53daa372c103_64232683 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/rezervace.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/calendar.css">
    <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Rezervace s údaji</title>
  </head>
  <body>

    <header>
          <div class="nav">
              <a class="link" href="index.php"><b>Home</b></a>
          </div>
    </header>

    <section>
      <br><br><br><br><br>
        <h1 class="alignLeft col-8 mx-auto"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getNazev();?>
</h1>
        <div class="imgCalendarContainer col-8 mx-auto">
          <div class="obrazekContainer">
            <img src="<?php echo $_smarty_tpl->tpl_vars['kolo']->value->getImg();?>
" alt="DodatIMG">
          </div>
          <div class="calendar">
            <div class="month">
              <ul>
                <li class="prev" onclick="prevMonth()">&#10094;</li>
                <li class="next" onclick="nextMonth()">&#10095;</li>
                <li id="monthAndYear"></li>
              </ul>
            </div>
            <ul id="weekDays"></ul>
            <ul id="monthDays"></ul>
            <div class="calendarFooter">
              <div class="footerRow">
                <div class="vybraneDnyHeader">Vybrané dny: </div>
                <div id="vybraneDny">Není vybráno žádné datum.</div>
              </div>
              <div class="footerRow">
                <div class="cenaHeader">Cena: </div>
                <div id="cena">0 Kč</div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <br>
    <span id="zabraneDny" style="display:none;"><?php echo $_smarty_tpl->tpl_vars['zabraneDny']->value;?>
</span>
    <section class="kontaktniUdajeBg col-8 mx-auto">
      <?php ob_start();
echo $_smarty_tpl->tpl_vars['error']->value;
$_prefixVariable1 = ob_get_clean();
if (($_prefixVariable1 == 1)) {?>
      <div class="col-8 mx-auto errorMsg">Rezervace se nezdařila. Zkuste to znovu.</div>
      <?php }?>
      <br><br>
      <h1>Kontaktní údaje</h1>
      <br><br>
      <form action="php/reservation.php" method="post">
      <div class="kontaktniUdaje">
        <div class="row">
            <input name="jmeno" class="formInput" placeholder="Jméno ..." required>
            <input name="prijmeni" class="formInput" placeholder="Příjmení ..." required>
            <input name="email" type="email" class="formInput" placeholder="Email (xxx@xxx.xx)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
            <input name="telefon" type="tel" class="formInput" placeholder="Telefon (+xxx xxx xxx xxx)" pattern="[+][0-9]{3} [0-9]{3} [0-9]{3} [0-9]{3}" required>
        </div>

        <div class="row">
            <input name="obcanka" class="formInput" placeholder="Číslo občanského průkazu ..." required>
            <input name="ridicak" class="formInput" placeholder="Číslo řidičského průkazu ..." required>
        </div>
<br>
        <div class="row">
            <input name="stat" class="formInput2" placeholder="Stát ..." required>
            <input name="obec" class="formInput2" placeholder="Obec ..." required>
            <input name="ulice" class="formInput2" placeholder="Ulice ..." required>
            <input name="cisloPopisne" class="formInput2" placeholder="Č.P." required>
            <input type="number" name="psc" class="formInput2" placeholder="PSČ ..." required>
            <div class="confirmContainer">
              <div class="row">
                <p><b>Všechna</b> pole jsou povinná</p>
              </div>
              <div class="row">
                <label class="lol">Souhlasím s obchodními podmínkami
                  <input type="checkbox" required>
                  <span class="checkmark"></span>
                  <a class="obchodniPodminky" target="_blank" href="http://localhost/pujcovna_kol/#cenik">?</a>
                </label>
              </div>
            </div>
        </div>

        <input type="hidden" name="cena" value="">
        <input type="hidden" name="vybraneDny" value="" required>
        <input type="hidden" name="idKola" value="<?php echo $_smarty_tpl->tpl_vars['kolo']->value->getId();?>
">

        <input type="submit" value="Odeslat" class="button">
        </div>
      </form>

    <br><br>
    </section>
    <br>
    <section class="technicke_parametry col-8 mx-auto"><br>
      <h1>Technické parametry</h1>
      <br>
    <div class="container">
      <div class="row a nad">
        <div class="col-6" style="margin: auto;">
          <a class="parametr"><b>Název kola: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getNazev();?>
</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Pohlaví: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getPohlavi();?>
</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Velikost kol: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getVelikostKol();?>
"</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Velikost baterie: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getBaterie();?>
Ah</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Umístění motoru: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getMotor();?>
</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Nosnost</b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Až <?php echo $_smarty_tpl->tpl_vars['kolo']->value->getNosnost();?>
kg</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Počet převodů: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getPrevody();?>
</a>
        </div>
      </div>
    </div>
    <br><br>
  </section>
<br>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  <?php echo '<script'; ?>
 src="js/calendar.js"><?php echo '</script'; ?>
>
  </body>
</html>
<?php }
}
