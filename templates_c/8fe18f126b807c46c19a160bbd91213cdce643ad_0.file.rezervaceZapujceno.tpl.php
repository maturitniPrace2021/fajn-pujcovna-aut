<?php
/* Smarty version 3.1.33, created on 2020-02-03 21:35:32
  from 'C:\Program Files (x86)\EasyPHP-Devserver-17\eds-www\pujcovna_kol\templates\rezervaceZapujceno.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e388414c3c9f2_57251556',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8fe18f126b807c46c19a160bbd91213cdce643ad' => 
    array (
      0 => 'C:\\Program Files (x86)\\EasyPHP-Devserver-17\\eds-www\\pujcovna_kol\\templates\\rezervaceZapujceno.tpl',
      1 => 1580760876,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e388414c3c9f2_57251556 (Smarty_Internal_Template $_smarty_tpl) {
?><html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Rezervace</title>
    <link rel="stylesheet" href="../css/kalendar.css">
    <link rel="stylesheet" href="../css/rezervace.css">
    <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">t">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  </head>
  <body>

    <header>
      <div class="container barva">
        <div class="row">
          <div class="col">
          </div>
          <div class="col">
          </div>
          <div class="col-9 odkazy" >
            <div class="row">
              <div class="col-3">
                <a class="nav-link" href="../index.php"><b>Hlavní strana</b></a>
              </div>
              <div class="col-3">
                <a class="nav-link" href="#cenik"><b>Ceník</b></a>
              </div>
              <div class="col-3">
                <a class="nav-link" href="#hodnoceni"><b>Hodnocení</b></a>
              </div>
              <div class="col-3">
                <a class="nav-link" href="#kontakt"><b>Kontakt</b></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>


    <section>
      <br><br><br><br><br>
        <div class="container">
          <div class="row">
              <div class="col-5 obrazek">
                <img class="obr" src="../img/other_img/loading.jpg" alt="DodatIMG">
              </div>
              <div class="col-1">
              </div>
              <div class="col-6">
              <div class="row">
                <div class="col-6 kalendar">
                  <div class="date-picker">
                    <div class="selected-date"></div>

                    <div class="dates">
                      <div class="month">
                        <div class="arrows prev-mth">&lt;</div>
                          <div class="mth"></div>
                          <div class="arrows next-mth">&gt;</div>
                        </div>

                        <div class="days"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 kalendar">

                    <div class="date-picker">
                      <div class="selected-date"></div>

                      <div class="dates">
                        <div class="month">
                          <div class="arrows prev-mth">&lt;</div>
                            <div class="mth"></div>
                            <div class="arrows next-mth">&gt;</div>
                          </div>

                          <div class="days"></div>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
            </div>
            </div>
    </section>
<br><br><br><br><br>


    <section class="technicke_parametry">
      <br>
      <h1>Technické parametry</h1>
      <br>
    <div class="container">
      <div class="row a nad">
        <div class="col-6">
          <a class="parametr"><b>Název kola: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Rok výroby: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Velikost kol: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Velikost baterie: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Umístění motoru: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Nosnost</b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row a">
        <div class="col-6">
          <a class="parametr"><b>Počet převodů: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
<br>
      <div class="row b">
        <div class="col-6">
          <a class="parametr"><b>Pohlaví: </b></a>
        </div>
        <div class="col-6 od">
          <a class="parametr">Odpověd</a>
        </div>
      </div>
    </div>
    <br><br>
  </section>

        <?php echo '<script'; ?>
>
          const date_picker_element = document.querySelector('.date-picker');
          const selected_date_element = document.querySelector('.date-picker .selected-date');
          const dates_element = document.querySelector('.date-picker .dates');
          const mth_element = document.querySelector('.date-picker .dates .month .mth');
          const next_mth_element = document.querySelector('.date-picker .dates .month .next-mth');
          const prev_mth_element = document.querySelector('.date-picker .dates .month .prev-mth');
          const days_element = document.querySelector('.date-picker .dates .days');

          const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

          let date = new Date();
          let day = date.getDate();
          let month = date.getMonth();
          let year = date.getFullYear();

          let selectedDate = date;
          let selectedDay = day;
          let selectedMonth = month;
          let selectedYear = year;

          mth_element.textContent = months[month] + ' ' + year;

          selected_date_element.textContent = formatDate(date);
          selected_date_element.dataset.value = selectedDate;

          populateDates();

          // EVENT LISTENERS
          date_picker_element.addEventListener('click', toggleDatePicker);
          next_mth_element.addEventListener('click', goToNextMonth);
          prev_mth_element.addEventListener('click', goToPrevMonth);

          // FUNCTIONS
          function toggleDatePicker (e) {
          	if (!checkEventPathForClass(e.path, 'dates')) {
          		dates_element.classList.toggle('active');
          	}
          }

          function goToNextMonth (e) {
          	month++;
          	if (month > 11) {
          		month = 0;
          		year++;
          	}
          	mth_element.textContent = months[month] + ' ' + year;
          	populateDates();
          }

          function goToPrevMonth (e) {
          	month--;
          	if (month < 0) {
          		month = 11;
          		year--;
          	}
          	mth_element.textContent = months[month] + ' ' + year;
          	populateDates();
          }

          function populateDates (e) {
          	days_element.innerHTML = '';
          	let amount_days = 31;

          	if (month == 1) {
          		amount_days = 28;
          	}

          	for (let i = 0; i < amount_days; i++) {
          		const day_element = document.createElement('div');
          		day_element.classList.add('day');
          		day_element.textContent = i + 1;

          		if (selectedDay == (i + 1) && selectedYear == year && selectedMonth == month) {
          			day_element.classList.add('selected');
          		}

          		day_element.addEventListener('click', function () {
          			selectedDate = new Date(year + '-' + (month + 1) + '-' + (i + 1));
          			selectedDay = (i + 1);
          			selectedMonth = month;
          			selectedYear = year;

          			selected_date_element.textContent = formatDate(selectedDate);
          			selected_date_element.dataset.value = selectedDate;

          			populateDates();
          		});

          		days_element.appendChild(day_element);
          	}
          }

          // HELPER FUNCTIONS
          function checkEventPathForClass (path, selector) {
          	for (let i = 0; i < path.length; i++) {
          		if (path[i].classList && path[i].classList.contains(selector)) {
          			return true;
          		}
          	}

          	return false;
          }
          function formatDate (d) {
          	let day = d.getDate();
          	if (day < 10) {
          		day = '0' + day;
          	}

          	let month = d.getMonth() + 1;
          	if (month < 10) {
          		month = '0' + month;
          	}

          	let year = d.getFullYear();

          	return day + ' / ' + month + ' / ' + year;
          }





                <?php echo '</script'; ?>
>

  </body>
</html>
<?php }
}
