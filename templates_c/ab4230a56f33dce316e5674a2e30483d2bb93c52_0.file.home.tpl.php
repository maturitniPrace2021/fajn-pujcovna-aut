<?php
/* Smarty version 3.1.33, created on 2021-03-03 19:39:53
  from 'D:\xampp\htdocs\pujcovna aut\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_603fd7f95876e2_04034440',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ab4230a56f33dce316e5674a2e30483d2bb93c52' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna aut\\templates\\home.tpl',
      1 => 1614796791,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
  ),
),false)) {
function content_603fd7f95876e2_04034440 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>version="1.0" encoding="utf-8"<?php echo '?>';?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php echo '<script'; ?>
 src="js/ajax.js"><?php echo '</script'; ?>
>
    <title>Home</title>
  </head>
  <body>

  <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  <div class="filtery">
    <select id="znacka" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['znacka'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>

    <select id="karoserie" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['karoserie'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>

    <select id="palivo" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['palivo'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>

    <select id="prevodovka" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['prevodovka'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>

  <div id="wrapper">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['auta']->value, 'auto');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['auto']->value) {
?>
      <div class="item">
        <a href="detailAuta.php?id=<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
">
          <img src="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getImg();?>
" alt="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
">
          <div><?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
 <?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
</div>
        </a>
      </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </div>

  <div class="mapa" id="kontakt">
    <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d82507.32058743977!2d13.301883882223812!3d49.74178702397769!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470af1e5133d11b7%3A0x31b9406e3fc10b83!2zUGx6ZcWI!5e0!3m2!1scs!2scz!4v1611082662070!5m2!1scs!2scz" width="1300" height="360" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></center>
  </div>

  </body>
</html>
<?php }
}
