<?php
/* Smarty version 3.1.33, created on 2020-02-24 22:44:02
  from 'C:\Program Files (x86)\EasyPHP-Devserver-17\eds-www\pujcovna_kol\templates\pridatRecenzi.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5443a25fedb2_53358751',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41cfcaab3d42c2d26fcc80e627e4ec14171df296' => 
    array (
      0 => 'C:\\Program Files (x86)\\EasyPHP-Devserver-17\\eds-www\\pujcovna_kol\\templates\\pridatRecenzi.tpl',
      1 => 1582580641,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/footer.tpl' => 1,
  ),
),false)) {
function content_5e5443a25fedb2_53358751 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/pridatRecenzi.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    <title></title>
  </head>
  <body>
    <div class="col-6 mx-auto contentBg">
        <h1>Přidat recenzi</h1>
        <form action="/action_page.php" class="col-10 mx-auto">
          <div class="formContent">
            <label>Obsah recenze:</label><br>
            <textarea></textarea>
            <label>Autor: </label><br>
            <input type="text" class="autor" value="Doe"><br><br>
            <input type="submit" value="Submit">
          </div>
        </form>
    </div>
    <?php $_smarty_tpl->_subTemplateRender("file:../templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  </body>
</html>
<?php }
}
