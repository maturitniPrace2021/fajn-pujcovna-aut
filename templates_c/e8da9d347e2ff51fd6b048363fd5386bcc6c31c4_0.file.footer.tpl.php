<?php
/* Smarty version 3.1.33, created on 2020-02-24 22:48:54
  from '/home/users/elektrokolapujco/elektrokolapujcovna.9e.cz/web/templates/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5444c69cca20_44010113',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e8da9d347e2ff51fd6b048363fd5386bcc6c31c4' => 
    array (
      0 => '/home/users/elektrokolapujco/elektrokolapujcovna.9e.cz/web/templates/footer.tpl',
      1 => 1582580883,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e5444c69cca20_44010113 (Smarty_Internal_Template $_smarty_tpl) {
?><footer>

<br>
  <div class="row col-8 mx-auto">
    <div class="pata">
      <p>© 2019 Paviza&Stelzer<br>
          All rights reserved
      </p>
    </div>
    <div class="footerOdkazy">
      <a class="fa fa-facebook" href="https://www.facebook.com/elektrokolaplzen.cz/" target="_blank"></a>
      <a class="fa fa-instagram" href="https://www.instagram.com/elektrokolaplzen/" target="_blank"></a>
      <a class="fa fa-chrome" href="http://www.elektrokolaplzen.cz/" target="_blank"></a>
    </div>
  </div>
<br>
</footer>
<?php }
}
