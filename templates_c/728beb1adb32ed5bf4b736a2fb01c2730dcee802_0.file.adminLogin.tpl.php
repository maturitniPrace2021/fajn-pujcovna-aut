<?php
/* Smarty version 3.1.33, created on 2021-03-14 10:45:53
  from 'D:\xampp\htdocs\pujcovna aut - sass\templates\adminLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_604ddb514dcc20_77408254',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '728beb1adb32ed5bf4b736a2fb01c2730dcee802' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna aut - sass\\templates\\adminLogin.tpl',
      1 => 1615715136,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_604ddb514dcc20_77408254 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style/main/main.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
	<title>Přihlášení do administrace</title>
</head>
<body class="loginBody">
  

  <div class="loginContainer">
	<div class="loginNadpis">
		<h2>Login</h2>
	</div>
	

	<form class="loginForm" action="php/login.php" method="post">
		<div>
			<label>Username</label>
			<input type="text" name="username">
		</div>
		<div>
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<span>
			<button type="submit">Login</button>
			<a href="index.php">Home</a>
		</span>
	</form>
  </div>

</body>
</html>
<?php }
}
