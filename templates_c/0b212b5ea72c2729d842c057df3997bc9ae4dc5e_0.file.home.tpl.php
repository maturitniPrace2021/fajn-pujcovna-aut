<?php
/* Smarty version 3.1.33, created on 2020-01-09 17:11:56
  from 'C:\xampp\htdocs\SOC_Pujcovna_kol\pujcovna_kol\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e1750cc8cc4e4_49379642',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b212b5ea72c2729d842c057df3997bc9ae4dc5e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\SOC_Pujcovna_kol\\pujcovna_kol\\templates\\home.tpl',
      1 => 1578586313,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e1750cc8cc4e4_49379642 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
     <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
     <link rel="stylesheet" href="css/swiper.css">

     <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"><?php echo '</script'; ?>
>
     <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"><?php echo '</script'; ?>
>
     <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"><?php echo '</script'; ?>
>
     <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.js"><?php echo '</script'; ?>
>
     <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.min.js"><?php echo '</script'; ?>
>
     <?php echo '<script'; ?>
 async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v5.0"><?php echo '</script'; ?>
>
    <title>Home</title>
    <?php echo '<script'; ?>
>

    function showBikes() {
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
    }
  };

  var selects = getSelects();
  xhttp.open("GET", "php/ajaxFilter.php?filter_ids="+selects, true);
  xhttp.send();
}

  function getSelects() {

    var selects = [];

    var velikostRamu = document.getElementById("velikost_ramu");
    selects["velikost_ramu"] = velikostRamu.options[velikostRamu.selectedIndex].value;

    var velikostKol = document.getElementById("velikost_kol");
    selects["velikost_kol"] = velikostKol.options[velikostKol.selectedIndex].value;

    var baterie = document.getElementById("baterie");
    selects["kapacita"] = baterie.options[baterie.selectedIndex].value;

    var displej = document.getElementById("displej");
    selects["displej"] = displej.options[displej.selectedIndex].value;

    var motor = document.getElementById("motor");
    selects["motor"] = motor.options[motor.selectedIndex].value;

    var dojezd = document.getElementById("dojezd");
    selects["dojezd"] = dojezd.options[dojezd.selectedIndex].value;

    return selects;
}

<?php echo '</script'; ?>
>
  </head>
  <body>

    <header>
      <div class="container barva">
        <div class="row">
          <div class="col">
          </div>
          <div class="col">
          </div>
          <div class="col-6 odkazy" >
            <div class="row">
              <div class="col-3">
                <a class="nav-link" href="#cenik"><b>Ceník</b></a>
              </div>
              <div class="col-3">
                <a class="nav-link" href="#hodnoceni"><b>Hodnocení</b></a>
              </div>
              <div class="col-3">
                <a class="nav-link" href="#kontakt"><b>Kontakt</b></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>





      <form action="" method="post">

        <section class="services">
          <div class="content_services filtr">
            <div class="container">

              <div class="row lupa">
                <div class="col-2"></div>
                <div class="col-8"><textarea name="lupa" class="textarea" placeholder="Najděte kolo podle názvu..."></textarea></div>
                <div class="col-2"></div>
              </div>

              <div class="row ram velikost_kol">
                  <div class="col-2"></div>
                  <div class="col-4"><select id="velikost_ramu" class="textarea">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['velikost_ramu'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                     </select></div>
                  <div class="col-4"><select id="velikost_kol" class="textarea">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['velikost_kol'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                     </select></div>
                  <div class="col-2"></div>
              </div>

              <div class="row kapacita displej">
                  <div class="col-2"></div>

                  <div class="col-4 hidden"><select id="baterie" class="textarea" >
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['baterie'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


                                     </select></div>
                  <div class="col-4 hidden"><select id="displej" class="textarea">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['displej'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                     </select></div>
                  <div class="col-2"></div>
              </div>

              <div id="zkouska" class="row motor dojezd">
                  <div class="col-2"></div>

                  <div class="col-4 hidden"><select id="motor" class="textarea" >
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['motor'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                     </select></div>
                  <div class="col-4 hidden"><select id="dojezd" class="textarea">
                                        <option value="" type="text">Vyberte objem motoru</option>
                                        <option value="value1" type="text">Motor1</option>
                                     </select></div>


                  <div class="col-2"></div>
              </div>
              <div class="row kapacita displej">
                <div class="col-2"></div>

                <div class="col-8"><input type="submit" value="Rozšířená filtrace" id="btn1" class="button" onclick="showBikes()"></div>

                <?php echo '<script'; ?>
 src="js\javascript.js"><?php echo '</script'; ?>
>

                <div class="col-2"></div>
              </div>
              </div>
            </div>
            <br>
            <br>



      </section>

    </form>


<nav>
     <div class="swiper-container slider">
         <div class="swiper-wrapper">
           <div class="swiper-slide">Slide 1</div>
           <div class="swiper-slide">Slide 2</div>
           <div class="swiper-slide">Slide 3</div>
           <div class="swiper-slide">Slide 4</div>
           <div class="swiper-slide">Slide 5</div>
           <div class="swiper-slide">Slide 6</div>
           <div class="swiper-slide">Slide 7</div>
           <div class="swiper-slide">Slide 8</div>
         </div>
         <div class="swiper-pagination"></div>
         <div class="swiper-button-next"></div>
         <div class="swiper-button-prev"></div>
       </div>
    <?php echo '<script'; ?>
 src="js/swiper.js"><?php echo '</script'; ?>
>

  </nav>
  <br>
  <br>
<section id="cenik" class="cenik">


   <h1>Ceník</h1>
  <div class="cen">
    <p>První dva dny jsou za 500,-, zbylé dny zápůjčky jsou za 200,-</p>
      <p>Půjčení na víkend znamená vyzvednutí v pátek po 12 hod a vrácení do
          pondělí do 12 hod.Půl dne před a půl dne po termínu vypůjčení se
          nezapočítává do ceny půjčovného</p>
        <p><b>Kauce:</b> 10 000,-</p>
        <p>Při poničení, rozbití nebo ztrátě určí majitel výši škody a zákazník je povinnen
        ji zaplatit</p>
    </div>
    <br>
    <br>
    <br>

</section>

<form action="" method="post">
 <section id="hodnoceni" class="hod">
   <br>
 <h1>Hodnocení zákazníků</h1>
    <div class="swiper-container hodnoceni">
        <div class="swiper-wrapper">
          <div class="swiper-slide slideer">Recenze 1</div>
          <div class="swiper-slide slideer">Recenze 2</div>
          <div class="swiper-slide slideer">Recenze 3</div>
          <div class="swiper-slide slideer">Recenze 4</div>
          <div class="swiper-slide slideer">Recenze 5</div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
      </div>
   <?php echo '<script'; ?>
 src="js/swiper.js"><?php echo '</script'; ?>
>
   <br>
   <br>
   <br>
   <br>

</section>
</form>

<section id="kontakt" class="kontakt">
  <br>
<h1>Kontakt</h1>
    <div class="row">
      <div class="col-7">
        <a class="google">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1288.7995350673198!2d13.37933907740215!3d49.75598718091147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470af1f2875ddb9b%3A0xb50ae23a92c3f32!2zTmEgUm91ZG7DqSA2MywgMzAxIDAwIFBsemXFiCAxLVNldmVybsOtIFDFmWVkbcSbc3TDrQ!5e0!3m2!1scs!2scz!4v1578411076594!5m2!1scs!2scz" width="800" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </a>
      </div>
      <div class="col-5">
        <div class="fb-page fb" data-href="https://www.facebook.com/elektrokolaplzen.cz" data-tabs="timeline" data-width="" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/elektrokolaplzen.cz" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/elektrokolaplzen.cz">Elektrokolaplzeň</a></blockquote></div></br>
        <i class="fab fa-chrome ikona"></i><a>elektrokolaplzen.cz</a></br>
                          <i class="fas fa-envelope ikona"></i><a>info@elektrokolaplzen.cz</a></br>
                          <i class="fas fa-map-marker-alt ikona"></i><a>Na Roudné 63, Plzeň 1</a><br>
                                                     <a class="adresa2">Severní Předměstí, 301 00</a></br>
                        <i class="fas fa-phone-volume ikona"></i>  <a href="tel:603436164">+420 603 436 164 </a></br>
                                                                    <a class="tel2" href="tel:603817144">+420 603 817 144</a></br>
      </div>
    </div>


  </div>
<br>

</section>


<footer>

  <br>
  <div class="row pata">
    <div class="col-10">
      <p>© 2019 Paviza&Stelzer</p>
      <p>All rights reserved</p>
    </div>
    <div class="col-2 icon-bar">
      <a href="https://www.facebook.com/elektrokolaplzen.cz/" target="_blank" class="fa fa-facebook"></a>
      <a href="https://www.instagram.com/elektrokolaplzen/" target="_blank" class="fa fa-instagram"></a>
      <a href="http://www.elektrokolaplzen.cz/" target="_blank" class="fa fa-chrome"></a>
  </div>

  </div>

  <br>
</footer>





  </body>
</html>
<?php }
}
