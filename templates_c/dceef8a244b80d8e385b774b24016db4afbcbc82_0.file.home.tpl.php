<?php
/* Smarty version 3.1.33, created on 2021-01-19 23:04:03
  from 'D:\xampp\htdocs\auta pico\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_600757535a4ac3_24329627',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dceef8a244b80d8e385b774b24016db4afbcbc82' => 
    array (
      0 => 'D:\\xampp\\htdocs\\auta pico\\templates\\home.tpl',
      1 => 1611093843,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_600757535a4ac3_24329627 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>version="1.0" encoding="utf-8"<?php echo '?>';?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v5.0"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/script.js"><?php echo '</script'; ?>
>
    <title>Home</title>
  </head>
  <body>

  <div class="filtery">
    <select id="znacka" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['znacka'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>

    <select id="karoserie" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['karoserie'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>

    <select id="palivo" class="textarea" onchange="showCars()">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['filtery']->value['palivo'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>

  <div id="wrapper">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['auta']->value, 'auto');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['auto']->value) {
?>
      <div class="item">
         id: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
<br>
         Znacka: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
<br>
         Model: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
<br>
         Karoserie: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getKaroserie();?>
<br>
         Spotreba: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getSpotreba();?>
<br>
         Vybava: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getVybava();?>
<br>
         Palivo: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getPalivo();?>
<br>
         PocetMist: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getPocetMist();?>
<br>
         Cena: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
<br>
         Img: <?php echo $_smarty_tpl->tpl_vars['auto']->value->getImg();?>
<br>
      </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </div>

  </body>
</html>
<?php }
}
