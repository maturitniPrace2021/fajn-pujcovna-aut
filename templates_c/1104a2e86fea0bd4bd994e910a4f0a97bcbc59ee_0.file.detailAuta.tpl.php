<?php
/* Smarty version 3.1.33, created on 2021-03-03 12:17:06
  from 'D:\xampp\htdocs\pujcovna aut\templates\detailAuta.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_603f70323f3fd0_58997098',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1104a2e86fea0bd4bd994e910a4f0a97bcbc59ee' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna aut\\templates\\detailAuta.tpl',
      1 => 1614770183,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
  ),
),false)) {
function content_603f70323f3fd0_58997098 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>version="1.0" encoding="utf-8"<?php echo '?>';?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/detailAuta.css">
    <link rel="stylesheet" href="css/calendar.css">
    <link rel="stylesheet" href="css/header.css">
    <?php echo '<script'; ?>
 src='https://kit.fontawesome.com/a076d05399.js'><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src='https://kit.fontawesome.com/a076d05399.js'><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="js/detailAuta.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 defer src="js/calendar.js"><?php echo '</script'; ?>
>
    <title>Home</title>
  </head>
  <body>

  <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  <div class="container">
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['error']->value;
$_prefixVariable1 = ob_get_clean();
if (($_prefixVariable1 == 1)) {?>
    <div class="errorMsg">Rezervace se nezdařila. Vyberte den a zkuste to znovu.</div>
    <?php }?>
    <div class="imgInfo">
      <div class="imgContainer">
        <img src="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getImg();?>
" alt="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
"/>
      </div>
      
      <div class="informace">
        <div class="row">
          <span class="bold">Značka:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Model:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Karosérie:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getKaroserie();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Spotřeba:</span>  
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getSpotreba();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Výbava:</span>
          <i id="vybavaSipka" class='fas fa-arrow-circle-down' onclick="vybavaToggle();"></i>
          <div id="informaceVybava">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['auto']->value->getVybava(), 'jednaVybava');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jednaVybava']->value) {
?>
              <span>-<?php echo $_smarty_tpl->tpl_vars['jednaVybava']->value;?>
</span>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </div>
        </div>
        <div class="row">
          <span class="bold">Palivo:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPalivo();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Počet míst:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPocetMist();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Převodovka:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPrevodovka();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Cena:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
 Kč</span>
        </div>
      </div>
    </div>

    <div class="formularKalendar">
      <div class="formular">
        <form action="php/rezervace.php" method="post">
          <input type="hidden" name="idAuta" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
">
          <input name="jmeno" placeholder="Jméno ..." required>
          <input name="prijmeni" placeholder="Příjmení ..." required>
          <input name="email" type="email" placeholder="Email (xxx@xxx.xx)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
          <input name="telefon" type="tel" placeholder="Telefon (+xxx xxx xxx xxx)" pattern="[+][0-9]{3} [0-9]{3} [0-9]{3} [0-9]{3}" required>
          <input name="obcanka" placeholder="Číslo občanského průkazu ..." required>
          <input name="ridicak" placeholder="Číslo řidičského průkazu ..." required>
          <input name="stat" placeholder="Stát ..." required>
          <input name="obec" placeholder="Obec ..." required>
          <input name="ulice" placeholder="Ulice ..." required>
          <input name="cisloPopisne" placeholder="Č.P." required>
          <input type="number" name="psc" placeholder="PSČ ..." required>
          <input type="submit" value="Odeslat" class="button">
          <input type="hidden" name="cena" value="">
          <input type="hidden" name="vybraneDny" value="" required>
        </form>
      </div>

      <div class="calendar">
        <div class="month">
          <ul>
            <li class="prev" onclick="prevMonth()">&#10094;</li>
            <li class="next" onclick="nextMonth()">&#10095;</li>
            <li id="monthAndYear"></li>
          </ul>
        </div>
        <ul id="weekDays"></ul>
        <ul id="monthDays"></ul>
        <div class="calendarFooter">
          <div class="footerRow">
            <div class="vybraneDnyHeader">Vybrané dny: </div>
            <div id="vybraneDny">Není vybráno žádné datum.</div>
          </div>
          <div class="footerRow">
            <div class="cenaHeader">Cena: </div>
            <div id="cena">0 Kč</div>
          </div>
        </div>
      </div>
      <span id="zabraneDny" style="display:none;"><?php echo $_smarty_tpl->tpl_vars['zabraneDny']->value;?>
</span>
      <input type="hidden" name="idKola" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
">
      <input type="hidden" name="cenaAuta" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
">

    </div>
    
  </div>

  


  </body>
</html>
<?php }
}
