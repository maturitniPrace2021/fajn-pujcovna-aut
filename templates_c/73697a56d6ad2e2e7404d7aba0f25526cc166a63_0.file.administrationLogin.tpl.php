<?php
/* Smarty version 3.1.33, created on 2020-12-01 19:50:08
  from 'D:\xampp\htdocs\pujcovna kol zaloha\templates\administrationLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5fc6906056cdc7_88129807',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '73697a56d6ad2e2e7404d7aba0f25526cc166a63' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna kol zaloha\\templates\\administrationLogin.tpl',
      1 => 1582584770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fc6906056cdc7_88129807 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/administrationLogin.css">
</head>
<body>
  <title>Přihlášení do administrace</title>
  <div class="header">
  	<h2>Login</h2>
  </div>
  <form action="php/login.php" method="post">
  	<div class="input-group">
  		<label>Username</label>
  		<input type="text" name="username">
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password">
  	</div>
  	<div class="input-group buttons">
  		<button type="submit" class="button submit" name="login_btn">Login</button>
      <a class="button home" href="index.php">Home</a>
  	</div>
  </form>
</body>
</html>
<?php }
}
