<?php
/* Smarty version 3.1.33, created on 2020-02-24 20:33:46
  from 'C:\Program Files (x86)\EasyPHP-Devserver-17\eds-www\pujcovna_kol\templates\administrationLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e54251a6e5384_99420079',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6283fdd818b9b0b70fbbdde00ad2836cb79cd4a8' => 
    array (
      0 => 'C:\\Program Files (x86)\\EasyPHP-Devserver-17\\eds-www\\pujcovna_kol\\templates\\administrationLogin.tpl',
      1 => 1582572826,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e54251a6e5384_99420079 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/administrationLogin.css">
</head>
<body>
  <div class="header">
  	<h2>Login</h2>
  </div>
  <form action="php/login.php" method="post">
  	<div class="input-group">
  		<label>Username</label>
  		<input type="text" name="username">
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password">
  	</div>
  	<div class="input-group buttons">
  		<button type="submit" class="button submit" name="login_btn">Login</button>
      <a class="button home" href="index.php">Home</a>
  	</div>
  </form>
</body>
</html>
<?php }
}
