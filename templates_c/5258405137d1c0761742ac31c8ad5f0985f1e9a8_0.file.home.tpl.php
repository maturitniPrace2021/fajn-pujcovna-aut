<?php
/* Smarty version 3.1.33, created on 2020-02-24 21:56:30
  from 'C:\Program Files (x86)\EasyPHP-Devserver-17\eds-www\pujcovna_kol\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e54387e476715_80029066',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5258405137d1c0761742ac31c8ad5f0985f1e9a8' => 
    array (
      0 => 'C:\\Program Files (x86)\\EasyPHP-Devserver-17\\eds-www\\pujcovna_kol\\templates\\home.tpl',
      1 => 1582577788,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e54387e476715_80029066 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>version="1.0" encoding="utf-8"<?php echo '?>';?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Exo+2&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://unpkg.com/swiper/js/swiper.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v5.0"><?php echo '</script'; ?>
>
    <title>Home</title>
    <?php echo '<script'; ?>
>

		function showBikes() {
		var xhttp;
		xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
          //prijeti kol a jejich konvertovani na JSON
        var kolaArray = JSON.parse(this.responseText);
          //pro kazde kolo se vytvori stejny div jako pri nacteni stranky
        var text = "";
    		for (i = 0; i < kolaArray.length; i++) {

          text += "<div class='item'>";
          text += "<a href='kontaktniUdaje.php?id="+kolaArray[i]['id']+"'>";
          text += "<p class='nazevKola'>"+kolaArray[i]['nazev']+"</p>";
          text += "<img src='"+kolaArray[i]['img']+"' alt='"+kolaArray[i]['nazev']+"'>";
          text += "</a>";
          text += "</div>";
    		}
          //pokud se nevrati zadna kola
        if(kolaArray.length == 0) {
          text += "<h2 class='noBikes'>Nebyla nalezena žádná kola odpovídající filterům</h2>";
          text += "<img class='noBikesImg' src='img/other_img/sad_face.png' alt='sad_face:('>";
        }
          //prirazeni divu s kolama, nebo textu "zadna kola" do wrapperu
        document.getElementById("wrapper").innerHTML = text;
      }
  	}
      //kod pod timhle komentarem se vykona jako prvni, kod nad se vykona az po prijeti dat z ajaxFilter.php
      //get selects
  	var selects = getSelects();
      //poslani selectu jako parametr na stranku ajaxFilter.php
  	xhttp.open("GET", "php/ajaxFilter.php?filters="+JSON.stringify(selects), true);
  	xhttp.send();
		}

    var filtersOn = false;
    function toggleFilters() {
      if(filtersOn) {
        document.getElementById("filtersContainer").style = "display: none;";
        document.getElementById("toggleFiltersButton").innerHTML = "Zobrazit filtery";
      } else {
        document.getElementById("filtersContainer").style = "display: block;";
        document.getElementById("toggleFiltersButton").innerHTML = "Skrýt filtery";
      }

      filtersOn = !filtersOn;
    }

    function checkSelects() {

      var selects = getSelects();
      var filterSet = false;

      for (filter in selects) {
        if(selects[filter] != 0) {
          filterSet = true;
          break;
        }
      }

      if(filterSet) {
        document.getElementById("filterButton").innerHTML = "Zobrazit filtrovaná kola";
      } else {
        document.getElementById("filterButton").innerHTML = "Zobrazit všechna kola";
      }
    }

		function getSelects() {

      //selectnuci filterů
		var pohlavi = document.getElementById("pohlavi");
		var velikostKol = document.getElementById("velikost_kol");
		var baterie = document.getElementById("baterie");
    var motor = document.getElementById("motor")
		var nosnost = document.getElementById("nosnost");
		var prevody = document.getElementById("prevody");
    var nazev = document.getElementById("nazev");
      //vytvoreni javascript object selects a přiřazení hodnot filterů
		var selects = {
			pohlavi: pohlavi.options[pohlavi.selectedIndex].value,
			velikost_kol: velikostKol.options[velikostKol.selectedIndex].value,
			baterie: baterie.options[baterie.selectedIndex].value,
      motor: motor.options[motor.selectedIndex].value,
			nosnost: nosnost.options[nosnost.selectedIndex].value,
			prevody: prevody.options[prevody.selectedIndex].value,
      nazev: nazev.value
		}

		return selects;
		}

<?php echo '</script'; ?>
>
  </head>
  <body>

        <section class="services col-8 mx-auto">
          <div class="upperContainer">
            <div class="nav">
                <a href="#cenik"><b>Ceník</b></a>
                <a href="#hodnoceni"><b>Hodnocení</b></a>
                <a href="#kontakt"><b>Kontakt</b></a>
            </div>

            <form action="php/ajaxFilter.php" method="get">
              <div class="row">
                <textarea id="nazev" class="textarea" placeholder="Najděte kolo podle názvu..."></textarea>
              </div>
              <div id="filtersContainer" style="display:none;">
                <div class="row">
                    <div class="filtersCol1">
                      <select id="pohlavi" class="textarea" onchange="checkSelects()">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['pohlavi'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>

                    <div class="filtersCol2">
                      <select id="velikost_kol" class="textarea" onchange="checkSelects()">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['velikost_kol'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                        <?php if (($_smarty_tpl->tpl_vars['id']->value == 0)) {?>
                        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                        <?php } else { ?>
                        <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>Velikost kol <?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
"</option>
                        <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>
                </div>

                <div class="row">
                    <div class="filtersCol1">
                      <select id="baterie" class="textarea" onchange="checkSelects()">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['baterie'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                        <?php if (($_smarty_tpl->tpl_vars['id']->value == 0)) {?>
                           <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                        <?php } else { ?>
                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>Velikost baterie <?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
Ah</option>
                        <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>

                    <div class="filtersCol2">
                      <select id="motor" class="textarea" onchange="checkSelects()">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['motor'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                          <?php if (($_smarty_tpl->tpl_vars['id']->value == 0)) {?>
                             <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                          <?php } else { ?>
                            <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>Motor <?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                          <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>
                </div>

                <div class="row">
                    <div class="filtersCol1">
                      <select id="nosnost" class="textarea" onchange="checkSelects()">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['nosnost'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                        <?php if (($_smarty_tpl->tpl_vars['id']->value == 0)) {?>
                           <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                        <?php } else { ?>
                          <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>Maximální nosnost <?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
kg</option>
                        <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>

                   <div class="filtersCol2">
                     <select id="prevody" class="textarea" onchange="checkSelects()">
                         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['selectFilters']->value['prevody'], 'txt', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['txt']->value) {
?>
                         <?php if (($_smarty_tpl->tpl_vars['id']->value == 0)) {?>
                            <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                         <?php } else { ?>
                           <option value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>Počet převodů <?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</option>
                          <?php }?>
                         <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                      </select>
                    </div>
                </div>
              </div>

                <div class="row">
                  <button type="button" id="toggleFiltersButton" onclick="toggleFilters()">Zobrazit filtery</button>
                  <button type="button" id="filterButton" onclick="showBikes()">Zobrazit všechna kola</button>
                </div>
              <br>
              <br>

            </form>
          </div>
      </section>

<div class="contentBg col-8 mx-auto">
  <div id="wrapper">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kola']->value, 'kolo');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['kolo']->value) {
?>
      <div class="item">
        <a href="kontaktniUdaje.php?id=<?php echo $_smarty_tpl->tpl_vars['kolo']->value->getId();?>
">
          <div class="nazevKola"><?php echo $_smarty_tpl->tpl_vars['kolo']->value->getNazev();?>
</div>
          <img src="<?php echo $_smarty_tpl->tpl_vars['kolo']->value->getImg();?>
" alt="<?php echo $_smarty_tpl->tpl_vars['kolo']->value->getNazev();?>
"></img>
        </a>
      </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </div>

 <a name="cenik"><h1>Ceník</h1></a>
  <div class="cenik_text">
    <p>První dva dny jsou za 500,-, zbylé dny zápůjčky jsou za 200,-.</p>
    <p>Půjčení na víkend znamená vyzvednutí v pátek po 12 hod a vrácení do
        pondělí do 12 hod. Půl dne před a půl dne po termínu vypůjčení se
        nezapočítává do ceny půjčovného.
    </p>
    <p><b>Kauce:</b> 10 000,-</p>
        <p>Při poničení, rozbití nebo ztrátě určí majitel výši škody a zákazník je povinnen
        ji zaplatit.</p>
  </div>
  <br><br><br>

<section>
  <br><br><br>
    <div class="swiper-container hodnoceni">
       <a name="hodnoceni"><h1>Hodnocení zákazníků</h1></a>
        <div class="swiper-wrapper">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['recenzeArray']->value, 'recenze');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['recenze']->value) {
?>
            <div class="swiper-slide pozadiHodnoceni">
              <p><?php echo $_smarty_tpl->tpl_vars['recenze']->value['obsah'];?>
</p>

              <br>
              <h4><?php echo $_smarty_tpl->tpl_vars['recenze']->value['autor'];?>
</h4>
            </div>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>
   <?php echo '<script'; ?>
 src="js/swiper.js"><?php echo '</script'; ?>
>
   <br>
   <br>
   <br>
   <br>

</section>

<a name="kontakt"><h1>Kontakt</h1></a>

    <div class="row kontaktContainer">
      <div class="mapaContainer">
        <a>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1288.7995350673198!2d13.37933907740215!3d49.75598718091147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470af1f2875ddb9b%3A0xb50ae23a92c3f32!2zTmEgUm91ZG7DqSA2MywgMzAxIDAwIFBsemXFiCAxLVNldmVybsOtIFDFmWVkbcSbc3TDrQ!5e0!3m2!1scs!2scz!4v1578411076594!5m2!1scs!2scz"></iframe>
        </a>
      </div>

      <div class="kontaktFb">
        <div class="fb-page" data-href="https://www.facebook.com/elektrokolaplzen.cz" data-tabs="timeline" data-height="400px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
          <blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/elektrokolaplzen.cz">
            <a href="https://www.facebook.com/elektrokolaplzen.cz">Elektrokolaplzeň</a>
          </blockquote>
        </div>
      <br>

        <div class="kontaktText">
          <i class="fab fa-chrome"></i>
          <a href="http://www.elektrokolaplzen.cz/">elektrokolaplzen.cz</a><br>
          <i class="fas fa-envelope"></i>
          <span>info@elektrokolaplzen.cz</span><br>
          <i class="fas fa-map-marker-alt"></i>
          <span>Na Roudné 63, Plzeň 1</span><br>
          <span class="odsazeni">Severní Předměstí, 301 00</span><br>
          <i class="fas fa-phone-volume"></i>
          <span>Tel: +420 603 436 164</span><br>
          <span class="odsazeni">Tel: +420 603 817 144</span>
        </div>
      </div>
    </div>

  <br><br><br>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  </body>
</html>
<?php }
}
