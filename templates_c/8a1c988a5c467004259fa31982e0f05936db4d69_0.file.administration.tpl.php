<?php
/* Smarty version 3.1.33, created on 2021-03-04 09:42:25
  from 'D:\xampp\htdocs\pujcovna aut\templates\administration.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60409d7103ad87_43040554',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a1c988a5c467004259fa31982e0f05936db4d69' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna aut\\templates\\administration.tpl',
      1 => 1614847344,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60409d7103ad87_43040554 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="utf-8">
    <title>Administrace rezervace</title>
    <link rel="stylesheet" href="css/administration.css">
    <?php echo '<script'; ?>
 src="js/administration.js"><?php echo '</script'; ?>
>
</head>

<body>

    <nav>
        <ul>
            <div>
                <li onclick="displayForm(1);">Auta</li>
                <li onclick="displayForm(2);">Rezervace</li>
                <li onclick="displayForm(3);">Admins</li>
            </div>
            <li><a href="php/odhlaseni.php">Odhlásit</a></li>
        </ul>
    </nav>

    <form id="formAuta" action="php/updateDatabase.php" method="post">
        <div class="udaje">
            <table id="tableAuta">
                <tr>
                <th>Smazat</th>
                <th>id</th>
                <th>znacka</th>
                <th>model</th>
                <th>karoserie</th>
                <th>spotreba</th>
                <th>vybava</th>
                <th>palivo</th>
                <th>pocetMist</th>
                <th>prevodovka</th>
                <th>cena</th>
                <th>img</th>
                </tr>
            
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['auta']->value, 'auto');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['auto']->value) {
?>
                <tr>
                <td><input type="checkbox" name="delete[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
"></td>
                <td><input type="text" name="id[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
" readonly></td>
                <td><input type="text" name="znacka[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
" required></td>
                <td><input type="text" name="model[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
" required></td>
                <td><input type="text" name="karoserie[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getKaroserie();?>
" ></td>
                <td><input type="text" name="spotreba[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getSpotreba();?>
" ></td>
                <td><input type="text" name="vybava[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getVybava();?>
" ></td>
                <td><input type="text" name="palivo[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getPalivo();?>
" ></td>
                <td><input type="text" name="pocetMist[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getPocetMist();?>
" ></td>
                <td><input type="text" name="prevodovka[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getPrevodovka();?>
" ></td>
                <td><input type="text" name="cena[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
" ></td>
                <td><input type="text" name="img[]" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getImg();?>
" ></td>
                </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <input type="text" name="form" value="auta" style="display: none;">
            </table>
        </div>
        
        <input class="button" type="submit">
        <div>
            <input class="button" type="button" value="Přidat auto" onclick="addRowAuta()">
        </div>
    </form>

    <form id="formRezervace" action="php/updateDatabase.php" method="post">
        <div class="udaje">
            <table id="tableRezervace">
                <tr>
                    <th>Smazat</th>
                    <th>id</th>
                    <th>ID auta</th>
                    <th>Rezervovane dny</th>
                    <th>Cena</th>
                    <th>Jmeno</th>
                    <th>Prijmeni</th>
                    <th>Email</th>
                    <th>Telefon</th>
                    <th>Obcanka</th>
                    <th>Ridicak</th>
                    <th>Stat</th>
                    <th>Obec</th>
                    <th>Ulice</th>
                    <th>Cislo popisne</th>
                    <th>PSC</th>
                </tr>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rezervace']->value, 'jednaRezervace');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jednaRezervace']->value) {
?>
                <tr>
                    <td><input type="checkbox" name="delete[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['id'];?>
"></td>
                    <td><input type="text" name="id[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['id'];?>
" readonly></td>
                    <td><input type="number" name="ID_auta[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['ID_auta'];?>
"></td>
                    <td><input type="text" name="rezervovane_dny[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['rezervovane_dny'];?>
"></td>
                    <td><input type="number" name="cena[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['cena'];?>
"></td>
                    <td><input type="text" name="jmeno[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['jmeno'];?>
"></td>
                    <td><input type="text" name="prijmeni[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['prijmeni'];?>
"></td>
                    <td><input type="text" name="email[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['email'];?>
"></td>
                    <td><input type="text" name="telefon[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['telefon'];?>
"></td>
                    <td><input type="text" name="obcanka[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['obcanka'];?>
"></td>
                    <td><input type="text" name="ridicak[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['ridicak'];?>
"></td>
                    <td><input type="text" name="stat[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['stat'];?>
"></td>
                    <td><input type="text" name="obec[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['obec'];?>
"></td>
                    <td><input type="text" name="ulice[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['ulice'];?>
"></td>
                    <td><input type="text" name="cislo_popisne[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['cislo_popisne'];?>
"></td>
                    <td><input type="number" name="psc[]" value="<?php echo $_smarty_tpl->tpl_vars['jednaRezervace']->value['psc'];?>
"></td>
                </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <input type="text" name="form" value="rezervace" style="display:none;">

            </table>
        </div>
        <input class="button" type="submit">
        <div>
            <input class="button" type="button" value="Přidat rezervaci" onclick="addRowRezervace()">
        </div>
    </form>

    <form id="formAdmins" action="php/updateDatabase.php" method="post">
        <table id="tableAdmins">

            <tr>
              <th>Smazat</th>
              <th>id</th>
              <th>Username</th>
              <th>Password</th>
            </tr>
        
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['admins']->value, 'admin');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['admin']->value) {
?>
            <tr>
              <td><input type="checkbox" name="delete[]" value="<?php echo $_smarty_tpl->tpl_vars['admin']->value['id'];?>
"></td>
              <td><input type="text" name="id[]" value="<?php echo $_smarty_tpl->tpl_vars['admin']->value['id'];?>
" readonly></td>
              <td><input type="text" name="username[]" value="<?php echo $_smarty_tpl->tpl_vars['admin']->value['username'];?>
"></td>
              <td><input type="text" name="password[]" value="" placeholder="Zahashovane heslo"></td>
            </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <input type="text" name="form" value="admins" style="display: none;">
        
          </table>
        
          <input class="button" type="submit">
          <div><input class="button" type="button" value="Přidat uživatele" onclick="addRowAdmins()"></div>
    </form>


</body>

</html><?php }
}
