<?php
/* Smarty version 3.1.33, created on 2021-03-17 19:34:46
  from 'D:\xampp\htdocs\pujcovna_aut\templates\detailAuta.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60524bc6a92bd4_35793184',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '60f544f45ae9c59be9e0a075b47bfe1a1d1827c0' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna_aut\\templates\\detailAuta.tpl',
      1 => 1616006065,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
  ),
),false)) {
function content_60524bc6a92bd4_35793184 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>version="1.0" encoding="utf-8"<?php echo '?>';?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="icon" href="img/logo_male.png">
    <link rel="stylesheet" href="style/main/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php echo '<script'; ?>
 src="js/detailAuta.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 defer src="js/calendar.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 defer src="js/toggleNav.js"><?php echo '</script'; ?>
>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <title>Fajn-půjčovna aut</title>
  </head>
  <body>

  <?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php ob_start();
echo $_smarty_tpl->tpl_vars['mailError']->value;
$_prefixVariable1 = ob_get_clean();
if (($_prefixVariable1 == 1)) {?>
    <div class="mailFeedback mailFail">Rezervace se nezdařila. Vyberte den a zkuste to znovu.</div>
  <?php } else {
ob_start();
echo $_smarty_tpl->tpl_vars['mailSuccess']->value;
$_prefixVariable2 = ob_get_clean();
if (($_prefixVariable2 == 1)) {?>
    <div class="mailFeedback mailSuccess">Rezervace byla úspěšná. Na email Vám bylo odesláno potvrzení.</div>
  <?php }}?>

  <div class="container">

    <div class="imgInfo">
      <div class="imgContainer">
        <img src="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getImg();?>
" alt="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
"/>
      </div>
      
      <div class="informace">
        <div class="row">
          <span class="bold">Značka:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Model:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Karosérie:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getKaroserie();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Spotřeba:</span>  
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getSpotreba();?>
  l/100 km</span>
        </div>
        <div class="row">
          <span class="bold">Výbava:</span>
          <i id="vybavaSipka" class='fa fa-arrow-circle-o-down' onclick="vybavaToggle();"></i>
          <div id="informaceVybava">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['auto']->value->getVybava(), 'jednaVybava');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['jednaVybava']->value) {
?>
              <span>-<?php echo $_smarty_tpl->tpl_vars['jednaVybava']->value;?>
</span>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </div>
        </div>
        <div class="row">
          <span class="bold">Palivo:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPalivo();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Počet míst:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPocetMist();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Převodovka:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getPrevodovka();?>
</span>
        </div>
        <div class="row">
          <span class="bold">Cena:</span>
          <span><?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
 Kč</span>
        </div>
      </div>
    </div>

    <div class="formularKalendar">

      <div class="calendar">
        <div class="month">
          <ul>
            <li class="prev fa fa-arrow-left" onclick="prevMonth()"></li>
            <li id="monthAndYear"></li>
            <li class="next fa fa-arrow-right" onclick="nextMonth()"></li>
          </ul>
        </div>
        <ul id="weekDays"></ul>
        <ul id="monthDays"></ul>
        <div class="calendarFooter">
          <div class="footerRow">
            <div class="vybraneDnyHeader">Vybrané dny: </div>
            <div id="vybraneDny">Zatím jste nevybrali žádné datum.</div>
          </div>
          <div class="footerRow">
            <div class="cenaHeader">Cena: </div>
            <div id="cena">0 Kč</div>
          </div>
        </div>
      </div>

      <div class="formular">
        <form action="php/rezervace.php" method="post">
          <input type="hidden" name="idAuta" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getId();?>
">
          <input name="jmeno" placeholder="Jméno ..." required>
          <input name="prijmeni" placeholder="Příjmení ..." required>
          <input name="email" type="email" placeholder="Email (xxx@xxx.xx)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
          <input name="telefon" type="tel" placeholder="Telefon (+xxx xxx xxx xxx)" pattern="[+][0-9]{3} [0-9]{3} [0-9]{3} [0-9]{3}" required>
          <input name="obcanka" placeholder="Číslo občanského průkazu ..." required>
          <input name="ridicak" placeholder="Číslo řidičského průkazu ..." required>
          <input name="stat" placeholder="Stát ..." required>
          <input name="obec" placeholder="Obec ..." required>
          <input name="ulice" placeholder="Ulice ..." required>
          <input name="cisloPopisne" placeholder="Č.P." required>
          <input type="number" name="psc" placeholder="PSČ ..." required>
          <input type="submit" value="Odeslat" class="button">
          <input type="hidden" name="cena" value="">
          <input type="hidden" name="vybraneDny" value="">
          <input type="hidden" name="nazevAuta" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getZnacka();?>
 <?php echo $_smarty_tpl->tpl_vars['auto']->value->getModel();?>
">
          
        </form>
      </div>
      
      <span id="zabraneDny" style="display:none;"><?php echo $_smarty_tpl->tpl_vars['zabraneDny']->value;?>
</span>
      <input type="hidden" name="cenaAuta" value="<?php echo $_smarty_tpl->tpl_vars['auto']->value->getCena();?>
">

    </div>



  </div>

  </body>
</html>
<?php }
}
