<?php
/* Smarty version 3.1.33, created on 2021-01-29 16:51:28
  from 'D:\xampp\htdocs\pujcovna aut\templates\adminLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60142f007e9562_83954440',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aae97e3f2515453b620530534bf40f3d409a6591' => 
    array (
      0 => 'D:\\xampp\\htdocs\\pujcovna aut\\templates\\adminLogin.tpl',
      1 => 1611935488,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60142f007e9562_83954440 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/adminLogin.css">
</head>
<body>
  <title>Přihlášení do administrace</title>

  <div class="container">
	<div class="nadpis">
		<h2>Login</h2>
	</div>
	

	<form action="php/login.php" method="post">
		<div>
			<label>Username</label>
			<input type="text" name="username">
		</div>
		<div>
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<div id="buttons">
			<button type="submit" class="button">Login</button>
			<a class="button home" href="index.php">Home</a>
		</div>
	</form>
  </div>

</body>
</html>
<?php }
}
