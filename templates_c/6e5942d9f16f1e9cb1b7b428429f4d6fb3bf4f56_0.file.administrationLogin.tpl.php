<?php
/* Smarty version 3.1.33, created on 2020-02-25 00:00:35
  from '/home/users/elektrokolapujco/elektrokolapujcovna.9e.cz/web/templates/administrationLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5455936bd006_82761992',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e5942d9f16f1e9cb1b7b428429f4d6fb3bf4f56' => 
    array (
      0 => '/home/users/elektrokolapujco/elektrokolapujcovna.9e.cz/web/templates/administrationLogin.tpl',
      1 => 1582584771,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e5455936bd006_82761992 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="css/administrationLogin.css">
</head>
<body>
  <title>Přihlášení do administrace</title>
  <div class="header">
  	<h2>Login</h2>
  </div>
  <form action="php/login.php" method="post">
  	<div class="input-group">
  		<label>Username</label>
  		<input type="text" name="username">
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password">
  	</div>
  	<div class="input-group buttons">
  		<button type="submit" class="button submit" name="login_btn">Login</button>
      <a class="button home" href="index.php">Home</a>
  	</div>
  </form>
</body>
</html>
<?php }
}
