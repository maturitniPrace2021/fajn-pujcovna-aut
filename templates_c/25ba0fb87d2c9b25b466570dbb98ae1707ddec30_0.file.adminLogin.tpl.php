<?php
/* Smarty version 3.1.33, created on 2021-03-15 21:56:23
  from 'D:\Programy\Xamp\htdocs\pujcovna_aut\templates\adminLogin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_604fc9f7919bb9_49192144',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '25ba0fb87d2c9b25b466570dbb98ae1707ddec30' => 
    array (
      0 => 'D:\\Programy\\Xamp\\htdocs\\pujcovna_aut\\templates\\adminLogin.tpl',
      1 => 1615841341,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
  ),
),false)) {
function content_604fc9f7919bb9_49192144 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style/main/main.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="icon" href="img/logo_male.png">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
	<title>Přihlášení do administrace</title>
</head>
<body class="loginBody">

<?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  

  <div class="loginContainer">
	<div class="loginNadpis">
		<h2>Login</h2>
	</div>
	

	<form class="loginForm" action="php/login.php" method="post">
		<div>
			<label>Username</label>
			<input type="text" name="username">
		</div>
		<div>
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<span>
			<button type="submit">Login</button>
		</span>
	</form>
  </div>

</body>
</html>
<?php }
}
