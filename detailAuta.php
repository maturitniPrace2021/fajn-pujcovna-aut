<?php
require 'php/db.php';
require 'libs/Smarty.class.php';

$smarty = new Smarty;
$db = new DB;
$rezervovaneDny = [];

$id = $_GET['id'];

if($id == null) {
  header("Location: index.php");
}

$sql = "SELECT * FROM auta WHERE id = " . $id;
$vybraneAuto = $db->getCars($sql)[0];

$rezervace = $db->getReservations();

foreach($rezervace as $row) {
  if($row['ID_auta'] == $id) {
    $dny = explode(",", $row['rezervovane_dny']);
    foreach($dny as $jednotliveDny) {
      array_push($rezervovaneDny, $jednotliveDny);
    }
  }
}

$rezervovaneDnyText = "";
for($i = 0; $i < count($rezervovaneDny); $i++) {
  if($i == 0) {
    $rezervovaneDnyText .= $rezervovaneDny[$i];
  } else {
    $rezervovaneDnyText .= "," . $rezervovaneDny[$i];
  }
}

$smarty->assign("mailError", isset($_GET['mailError']));
$smarty->assign("mailSuccess", isset($_GET['mailSuccess']));
$smarty->assign("zabraneDny", $rezervovaneDnyText);
$smarty->assign("auto", $vybraneAuto);
$smarty->display('detailAuta.tpl');
