-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 29. led 2021, 15:19
-- Verze serveru: 10.4.13-MariaDB
-- Verze PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `fajn_pujcovna_aut`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `rezervace`
--

CREATE TABLE `rezervace` (
  `id` int(11) NOT NULL,
  `ID_auta` int(11) NOT NULL,
  `rezervovane_dny` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `cena` int(11) NOT NULL,
  `jmeno` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `prijmeni` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `telefon` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `obcanka` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ridicak` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `stat` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `obec` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ulice` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `cislo_popisne` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `psc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `rezervace`
--

INSERT INTO `rezervace` (`id`, `ID_auta`, `rezervovane_dny`, `cena`, `jmeno`, `prijmeni`, `email`, `telefon`, `obcanka`, `ridicak`, `stat`, `obec`, `ulice`, `cislo_popisne`, `psc`) VALUES
(25, 2, '?', 0, 'Daniel', 'Pavíza', 'email@neco.cz', '+111 111 111 111', 'cislo obcanky', 'cislo ridicaku', 'stat', 'obec', 'ulice', 'cislo popisne', 32300);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
