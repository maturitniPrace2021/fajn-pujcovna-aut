-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Počítač: sql.endora.cz:3310
-- Vytvořeno: Stř 17. bře 2021, 20:33
-- Verze serveru: 5.6.45-86.1
-- Verze PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `autopujcovna`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `auta`
--

CREATE TABLE `auta` (
  `id` int(11) NOT NULL,
  `znacka` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `karoserie` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `spotreba` int(11) NOT NULL,
  `vybava` varchar(500) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `palivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `pocetMist` int(11) NOT NULL,
  `prevodovka` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `cena` int(11) NOT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;

--
-- Vypisuji data pro tabulku `auta`
--

INSERT INTO `auta` (`id`, `znacka`, `model`, `karoserie`, `spotreba`, `vybava`, `palivo`, `pocetMist`, `prevodovka`, `cena`, `img`) VALUES
(1, 'Audi', 'Q5', 'SUV', 11, 'ABS, alarm, asistent rozjezdu do kopce	ESP, tažné zařízení,autorádio, vyhřívaná sedadla, bluetooth, palubní počítač	xenony, el. ovládání oken, pohon 4x4', 'Benzín', 5, 'Automat', 1000, 'img\\car_1\\audi_q5.png'),
(2, 'Audi', 'R8', 'Kupé', 15, 'ABS, kožená sedadla, posilovač řízení, ASR, automatické svícení, LED denní svícení, satelitní navigace,autorádio, centrální zamykání, multifunkční volant, tempomat,dálkové centrální zamykání, nastavitelná sedadla, tónovaná skla', 'Benzín', 2, 'Automat', 2500, 'img\\car_2\\audi_r8.jpg'),
(3, 'Audi', 'RS7', 'Hatchback', 10, 'ABS, adaptivní tempomat, isofix, alarm, ASR, asistent rozjezdu do kopce, kožená sedadla, satelitní navigace,bluetooth, CD přehrávač	litá kola, senzor tlaku v pneumatikách,multifunkční volant, tempomat, denní svícení, parkovací asistent,parkovací senzory zadní', 'Benzín', 5, 'Automat', 2600, 'img\\car_3\\audi_rs7.png'),
(4, 'BMW', 'i8', 'Kupé', 2, 'alarm, Laserová světla, senzor tlaku v pneumatikách,autorádio, litá kola, ukazatel rychlostních limitů,bezklíčkové ovládání, ostřikovače světlometů, vyhřívaná sedadlasatelitní navigace', 'Hybridní', 4, 'Automat', 3000, 'img\\car_4\\bmw_i8.png'),
(5, 'BMW', 'M340i', 'Sedan', 8, 'ABS, ESP, alarm, ASR, satelitní navigace,asistent rozjezdu do kopce, kožená sedadla,automatická klimatizace	kožené, čalounění, senzor tlaku v pneumatikách, automatické svícení,LED denní svícení, sledování jízdního pruhu,autorádio, LED světlomety, sportovní podvozekbezklíčkové ovládání, bluetooth, Start/Stop systém', 'Benzín', 5, 'Manual', 1000, 'img\\car_5\\bmw_m.png'),
(6, 'BMW', 'X5', 'SUV', 7, 'alarm, LED denní svícení, senzor opotřebení brzd. destiček, autorádio, litá kola, senzor tlaku v pneumatikách, bezdrátová nabíječka mobilních telefonů, multifunkční volant, ukazatel rychlostních limitů SLIF, bezklíčkové ovládání, pohon 4 x 4, závěsné zařízení v TP, el. seřiditelná sedadla, regulace tuhosti podvozku', 'Nafta', 5, 'Automat', 2500, 'img\\car_6\\bmw_x5.png'),
(7, 'Ferrari', '458 spider', 'Kupé', 11, 'ABS, kožené čalounění, satelitní navigace, alarm, kožené potahy, senzor opotřebení brzd. destiček, multifunkční volant, senzor tlaku v pneumatikách, bluetooth, natáčecí světlomety, sportovní podvozek, centrální zamykání, ostřikovače světlometů, sportovní sedadla, dálkové centrální zamykání, palubní počítač, tempomat', 'Benzín', 2, 'Automat', 10000, 'img\\car_7\\ferrari_458spider.png'),
(8, 'Ford', 'Mustang', 'Kupé', 12, 'ABS, ESP, satelitní navigace, alarm, imobilizér, senzor tlaku v pneumatikách, automatická klimatizace, kožené potahy, sportovní podvozek, bezklíčkové ovládání, litá kola, sportovní sedadla, bluetooth, mlhovky, tempomat, centrální zamykání, nastavitelný volant, venkovní teploměr, dálkové centrální zamykání, palubní počítač, vyhřívaná sedadla', 'Benzín', 4, 'Automat', 5000, 'img\\car_8\\ford_mustang.png'),
(9, 'Hyundai', 'i20', 'Hatchback', 5, 'ABS, el. ovládání zrcátek, posilovač řízení, alarm, ESP, senzor tlaku v pneumatikách, Asistent pro vedení vozu v jízdních pruzích, imobilizér, sledování únavy řidiče, asistent rozjezdu do kopce, isofix, Start/Stop systém, automatické svícení, LED denní svícení, tempomat, bluetooth, USB, dálkové centrální zamykání, manuální klimatizace, venkovní teploměr', 'Benzín', 5, 'Manual', 500, 'img\\car_9\\hyundai_i20.png'),
(10, 'Hyundai', 'i30', 'Hatchback', 5, 'ABS, ESP, sledování únavy řidiče, Asistent pro vedení vozu v jízdních pruzích, imobilizér, Start/Stop systém, asistent rozjezdu do kopce, isofix, střešní nosič, automatické svícení, manuální klimatizace, tempomat, autorádio, Mobilní připojení, tónovaná skla, bluetooth, multifunkční volant, USB, dálkové centrální zamykání, nastavitelný volant, vyhřívaná zrcátka, deaktivace airbagu spolujezdce, nouzové brždění, výsuvné opěrky hlav, dělená zadní sedadla, originál autorádio', 'Benzín', 5, 'Manual', 700, 'img\\car_10\\hyundai_i30.png'),
(11, 'Hyundai', 'Sonata', 'Kupé', 6, 'ABS, ESP, nastavitelný volant, automatická klimatizace, imobilizér, palubní počítač, CD přehrávač, isofix, centrální zamykání, litá kola, ASR, dálkové centrální zamykání, tempomat, el. ovládání oken, mlhovky, venkovní teploměr, el. ovládání zrcátek, multifunkční volant, výškově nastavitelné sedadlo řidiče', 'Hybridní', 5, 'Automat', 600, 'img\\car_11\\hyundai_Sonata.png'),
(12, 'Hyundai', 'Tucson', 'SUV', 7, 'ABS, El. parkovací brzda, regulace při jízdě ze svahu, alarm, ESP, senzor tlaku v pneumatikách, Android Auto, imobilizér, sledování únavy řidiče, isofix, Start/Stop systém, Asistent pro vedení vozu v jízdních pruzích,LED denní svícení, střešní spoiler, asistent rozjezdu do kopce, malý kožený paket,  tempomat, Automatické přepínání dálkových světel, manuální klimatizace, tónovaná skla, parkovací asistent', 'Benzín', 5, 'Manual', 1500, 'img\\car_12\\hyundai_tucson.png'),
(13, 'Chevrolet', 'Camaro', 'Kupé', 10, 'ABS, ESP, posilovač řízení, alarm, hlídání mrtvého úhlu, regulace tuhosti podvozku, automatická klimatizace, imobilizér, senzor stěračů, bezklíčkové ovládání, kožené potahy, senzor tlaku v pneumatikách, bluetooth, litá kola, sportovní podvozek, centrální zamykání, mlhovky, tempomat, dálkové centrální zamykání, multifunkční volant, venkovní teploměr, denní svícení, nastavitelný volant, vyhřívaná sedadla, dvouzónová klimatizace, palubní počítač, vyhřívaná zrcátka', 'Benzín', 4, 'Automat', 4000, 'img\\car_13\\chevrolet_camaro.png'),
(14, 'Lamborghini', 'Huracán', 'Kupé', 15, 'ABS, LED světlomety, regulace tuhosti podvozku, autorádio, parkovací asistent, sportovní podvozek, bluetooth, parkovací kamera, sportovní sedadla, ESP, pohon 4x4', 'Benzín', 2, 'Automat', 20000, 'img\\car_14\\lamborghini.png'),
(15, 'Mazda', 'CX-3', 'SUV', 8, 'ABS, el. ovládání zrcátek, senzor stěračů, autorádio, el. sklopná zrcátka, start/Stop systém, bezklíčkové ovládání, ESP, tažné zařízení, bluetooth, imobilizér, tempomat, CD přehrávač, isofix, tónovaná skla, centrální zamykání, litá kola, USB, dálkové centrální zamykání, multifunkční volant, venkovní teploměr, deaktivace airbagu spolujezdce, nastavitelný volant, vyhřívaná zrcátka', 'Benzín', 5, 'Manual', 2500, 'img\\car_15\\mazda_cx3.png'),
(16, 'Škoda', 'Citigo', 'Hatchback', 6, 'ABS, isofix, palubní počítač, CD přehrávač, litá kola, posilovač řízení, dálkové centrální zamykání, manuální klimatizace, vyhřívaná zrcátka, el. ovládání oken, mlhovky, zámek řadící páky, el. ovládání zrcátek, nastavitelný volant', 'Benzín', 4, 'Manual', 1000, 'img\\car_16\\sk_citigo.jpg'),
(17, 'Škoda', 'Fabia', 'Kombi', 7, 'ABS, isofix, palubní počítač, CD přehrávač, litá kola, posilovač řízení, dálkové centrální zamykání, manuální klimatizace, vyhřívaná zrcátka, el. ovládání oken, mlhovky, zámek řadící páky, el. ovládání zrcátek, nastavitelný volantbluetooth, sportovní sedadla, zadní stěrač, parkovací senzory, střešní nosič', 'Benzín', 5, 'Manual', 1500, 'img\\car_17\\sk_Fabia.png'),
(18, 'Škoda', 'Karoq', 'SUV', 6, 'ABS, imobilizér, posilovač řízení, automatické svícení, isofix, ASR, dálkové centrální zamykání, klimatizovaná přihrádka, Start/Stop systém, denní svícení, mlhovky, USB, Dojezdové rezervní kolo, nastavitelná sedadla, vstup paměťové karty, el. ovládání oken, nastavitelný volant, vyhřívaná zrcátka, ESP, nouzové brždění', 'Benzín', 5, 'Automat', 1700, 'img\\car_18\\sk_karoq.png'),
(19, 'Škoda', 'Superb', 'Kombi', 8, 'ABS, isofix, senzor opotřebení brzd. destiček, alarm, klimatizovaná přihrádka, senzor stěračů, asistent rozjezdu do kopce, LED denní svícení, senzor tlaku v pneumatikách, automatické svícení, litá kola, sportovní sedadla, autorádio, mlhovky, start/Stop systém, multifunkční volant, tempomat, bluetooth, nastavitelná, sedadla, tónovaná skla, CD přehrávač, nastavitelný volant, USB, dálkové centrální zamykání, nouzové brždění, uzávěrka mezinápravového diferenciálu, deaktivace airbagu spol', 'Nafta', 5, 'Manual', 3000, 'img\\car_19\\sk_superb.png'),
(20, 'Volkswagen', 'Arteon', 'Liftback', 7, 'asistent rozjezdu do kopce, litá kola, sledování jízdního pruhu, automatická klimatizace, multifunkční volant, sledování únavy řidiče, imobilizér, parkovací kamera, Start/Stop systém, isofix, satelitní navigace, zatmavená zadní skla, LED světlomety, senzor tlaku v pneumatikách', 'Nafta', 5, 'Automat', 3000, 'img\\car_20\\vw_arteon.png'),
(21, 'Volkswagen', 'Passat', 'Kombi', 7, 'ABS, klimatizovaná přihrádka, senzor stěračů, alarm, LED denní svícení, sledování jízdního pruhu, asistent rozjezdu do kopce, LED světlomety, sledování únavy řidiče, automatické svícení, mlhovky, Start/Stop systém, bluetooth, multifunkční volant, tempomat, CD přehrávač, nastavitelná sedadla, tónovaná skla, dálkové centrální zamykání, nastavitelný volant, USB, deaktivace airbagu spolujezdce, originál autorádio, venkovní teploměr, dělená zadní sedadla, ostřikovače světlometů, vstup p', 'Benzín', 5, 'Manual', 2500, 'img\\car_21\\vw_passat.png'),
(22, 'Volkswagen', 'Polo', 'Hatchback', 5, 'asistent rozjezdu do kopce, ESP, senzor tlaku v pneumatikách, centrální zamykání, imobilizér, Start/Stop systém, el. ovládání oken, isofix, výškově nastavitelná sedadla, el. ovládání zrcátek, posilovač řízení, zadní stěrač', 'Benzín', 5, 'Manual', 1600, 'img\\car_22\\vw_polo.png'),
(23, 'Volkswagen', 'Tiguan', 'SUV', 8, 'ABS, alarm, asistent rozjezdu do kopce	ESP, tažné zařízení,autorádio, vyhřívaná sedadla, bluetooth, palubní počítač	xenony, el. ovládání oken, pohon 4x4', 'Nafta', 5, 'Manual', 2000, 'img\\car_23\\vw_tiguan.png');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `auta`
--
ALTER TABLE `auta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `auta`
--
ALTER TABLE `auta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
