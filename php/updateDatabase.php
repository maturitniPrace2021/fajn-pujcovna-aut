<?php
require 'db.php';
require '../libs/Smarty.class.php';
$db = new DB;
$data = $_POST;

if(!isset($data['id'])) {
  header("Location: ../administration.php");
  exit();
}


if($data['form'] == "auta") {
    
    $autaPred = $db->getCars();
    $autaPo = [];
    $auto = [];
    for($i = 0; $i < count($data['id']); $i++) {

        $auto['id'] = $data['id'][$i];
        $auto['znacka'] = $data['znacka'][$i];
        $auto['model'] = $data['model'][$i];
        $auto['karoserie'] = $data['karoserie'][$i];
        $auto['spotreba'] = $data['spotreba'][$i];
        $auto['vybava'] = $data['vybava'][$i];
        $auto['palivo'] = $data['palivo'][$i];
        $auto['pocetMist'] = $data['pocetMist'][$i];
        $auto['prevodovka'] = $data['prevodovka'][$i];
        $auto['cena'] = $data['cena'][$i];
        $auto['img'] = $data['img'][$i];

        array_push($autaPo, $auto);
    }

    if(isset($_POST['delete'])) {
        foreach($_POST['delete'] as $id) {

        $db->deleteCar($id);
    }
  }

    foreach($autaPo as $autoPo) {

    foreach($autaPred as $autoPred) {
    
        if($autoPred->getId() == $autoPo['id']) {
            if(
            $autoPred->getZnacka() !== $autoPo['znacka'] ||
            $autoPred->getModel() !== $autoPo['model'] ||
            $autoPred->getKaroserie() !== $autoPo['karoserie'] ||
            $autoPred->getSpotreba() !== $autoPo['spotreba'] ||
            $autoPred->getVybava() !== $autoPo['vybava'] ||
            $autoPred->getPalivo() !== $autoPo['palivo'] ||
            $autoPred->getPocetMist() !== $autoPo['pocetMist'] ||
            $autoPred->getPrevodovka() !== $autoPo['prevodovka'] ||
            $autoPred->getCena() !== $autoPo['cena'] ||
            $autoPred->getImg() !== $autoPo['img']
            
            ) {
            $db->updateCar($autoPo);
            }
            }
        }
        if($autoPo['id'] == "automaticky") {
        $db->insertCar($auto);
        }
  }

} else if ($data['form'] == "rezervace") {
    
    $rezervacePred = $db->getReservations();
    $rezervacePo = [];
    $rezervaceJedna = [];

    for($i = 0; $i < count($_POST['id']); $i++) {

        $rezervaceJedna['id'] = $data['id'][$i];
        $rezervaceJedna['ID_auta'] = $data['ID_auta'][$i];
        $rezervaceJedna['rezervovane_dny'] = $data['rezervovane_dny'][$i];
        $rezervaceJedna['cena'] = $data['cena'][$i];
        $rezervaceJedna['jmeno'] = $data['jmeno'][$i];
        $rezervaceJedna['prijmeni'] = $data['prijmeni'][$i];
        $rezervaceJedna['email'] = $data['email'][$i];
        $rezervaceJedna['telefon'] = $data['telefon'][$i];
        $rezervaceJedna['obcanka'] = $data['obcanka'][$i];
        $rezervaceJedna['ridicak'] = $data['ridicak'][$i];
        $rezervaceJedna['stat'] = $data['stat'][$i];
        $rezervaceJedna['obec'] = $data['obec'][$i];
        $rezervaceJedna['ulice'] = $data['ulice'][$i];
        $rezervaceJedna['cislo_popisne'] = $data['cislo_popisne'][$i];
        $rezervaceJedna['psc'] = $data['psc'][$i];
        array_push($rezervacePo, $rezervaceJedna);
    }

    if(isset($_POST['delete'])){
        foreach($_POST['delete'] as $id) {

            $db->deleteReservation($id);
        }
    }

    foreach($rezervacePo as $rezervacePoJedna) {

        foreach($rezervacePred as $rezervacePredJedna) {
    
        if($rezervacePredJedna['id'] == $rezervacePoJedna['id']) {
            if(
            $rezervacePredJedna['ID_auta'] !== $rezervacePoJedna['ID_auta'] ||
            $rezervacePredJedna['rezervovane_dny'] !== $rezervacePoJedna['rezervovane_dny'] ||
            $rezervacePredJedna['cena'] !== $rezervacePoJedna['cena'] ||
            $rezervacePredJedna['jmeno'] !== $rezervacePoJedna['jmeno'] ||
            $rezervacePredJedna['prijmeni'] !== $rezervacePoJedna['prijmeni'] ||
            $rezervacePredJedna['email'] !== $rezervacePoJedna['email'] ||
            $rezervacePredJedna['telefon'] !== $rezervacePoJedna['telefon'] ||
            $rezervacePredJedna['obcanka'] !== $rezervacePoJedna['obcanka'] ||
            $rezervacePredJedna['ridicak'] !== $rezervacePoJedna['ridicak'] ||
            $rezervacePredJedna['stat'] !== $rezervacePoJedna['stat'] ||
            $rezervacePredJedna['obec'] !== $rezervacePoJedna['obec'] ||
            $rezervacePredJedna['ulice'] !== $rezervacePoJedna['ulice'] ||
            $rezervacePredJedna['cislo_popisne'] !== $rezervacePoJedna['cislo_popisne'] ||
            $rezervacePredJedna['psc'] !== $rezervacePoJedna['psc']
            ) {
            $db->updateReservation($rezervacePoJedna);
            }
            }
        }
        if($rezervacePoJedna['id'] == "automaticky") {
        $db->insertReservation($rezervacePoJedna);
        }
    }
} else if ($data['form'] == "admins") {

    $adminsPred = $db->getAdmins();
    $adminsPo = [];
    $admin = [];
    for($i = 0; $i < count($_POST['id']); $i++) {

        $admin['id'] = $data['id'][$i];
        $admin['username'] = $data['username'][$i];
        $admin['password'] = $data['password'][$i];
        array_push($adminsPo, $admin);
    }
    
    if(isset($data['delete'])){
        foreach($data['delete'] as $id) {
            $db->deleteAdmin($id);
        }
    }
  foreach($adminsPo as $adminPo) {

    foreach($adminsPred as $adminPred) {
  
      if($adminPred['id'] == $adminPo['id']) {
        if($adminPred['username'] !== $adminPo['username']) {
          $db->updateAdmin($adminPo);
          }
        }
      }
    if($adminPo['id'] == "automaticky") {

      $adminPo['password'] = password_hash($adminPo['password'], PASSWORD_BCRYPT);
      $db->insertAdmin($adminPo);
    }
  }
    
}

header("Location: ../administration.php");
exit();
