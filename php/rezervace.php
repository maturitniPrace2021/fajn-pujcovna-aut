<?php
require '../libs/Smarty.class.php';
require 'db.php';
$smarty = new Smarty;
$db = new DB;

$postData = $_POST;
if($postData['vybraneDny'] == "") {
  header("Location: ../detailAuta.php?id=" . $postData['idAuta'] . "&mailError");
  exit();
}

$idAuta = $postData['idAuta'];
$vybraneDny = $postData['vybraneDny'];
$cena = $postData['cena'];
$jmeno = $postData['jmeno'];
$prijmeni = $postData['prijmeni'];
$email = $postData['email'];
$telefon = $postData['telefon'];
$obcanka = $postData['obcanka'];
$ridicak =$postData['ridicak'];
$stat = $postData['stat'];
$obec = $postData['obec'];
$ulice = $postData['ulice'];
$cisloPopisne = $postData['cisloPopisne'];
$psc = $postData['psc'];
$nazevAuta = $postData['nazevAuta'];

$db->reservation(
    $idAuta,
    $vybraneDny,
    $cena,
    $jmeno,
    $prijmeni,
    $email,
    $telefon,
    $obcanka,
    $ridicak,
    $stat,
    $obec,
    $ulice,
    $cisloPopisne,
    $psc
  );

  //MAIL ZAKAZNIKOVI
  $to = $email;
  $subject = "Rezervace Auta";
  
  $message = "
  <html>
      <head>
          <title>Rezervace Auta</title>
      </head>
          <body>
              <h3>Děkujeme za objednávku!</h3>
              <br>
              <p style='font-size: 18px;'>
                  Budeme se Vám snažit odpovědět co nejdříve. 
              </p>
              <p><b>Název auta: </b>$nazevAuta</p>
              <p><b>Rezervované dny: </b>$vybraneDny</p>
              <p><b>Cena: </b>$cena Kč</p>
          </body>
  </html>
  ";
  
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= 'From: <autopujcovna1@gmail.com>' . "\r\n";
  mail($to,$subject,$message,$headers);
  
  
      //MAIL MNĚ
  $to = "autopujcovna1@gmail.com";
  $subject = "Nová objednávka";
  
  $message = "
  <html>
  <head>
      <title>Nová objednávka</title>
  </head>
  <body>
      <h3>Nová rezervace</h3>
      <br>
      <p><b>Jméno: </b>$jmeno</p>
      <p><b>Příjmení: </b>$prijmeni</p>
      <p><b>Email: </b>$email</p>
      <p><b>Telefon: </b>$telefon</p>
      <p><b>Občanka: </b>$obcanka</p>
      <p><b>Řidičák: </b>$ridicak</p>
      <p><b>Stát: </b>$stat</p>
      <p><b>Obec: </b>$obec</p>
      <p><b>Ulice: </b>$ulice</p>
      <p><b>Číslo popisné: </b>$cisloPopisne</p>
      <p><b>PSČ: </b>$psc</p>
      <br>
      <p><b>ID auta: </b>$idAuta</p>
      <p><b>Název auta: </b>$nazevAuta</p>
      <p><b>Zarezervované dny: </b>$vybraneDny</p>
      <p><b>Cena: </b>$cena</p>
  </body>
  </html>
  ";
  
  mail($to,$subject,$message,$headers);


  header("Location: ../detailAuta.php?id=" . $postData['idAuta'] . "&mailSuccess");