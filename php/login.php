<?php

require 'db.php';
$db = new DB;

$username = $_POST['username'];
$password = $_POST['password'];
$hash = $db->getAdminHash($username);

if (password_verify($password, $hash)) {

  if (session_status() == PHP_SESSION_NONE) {
      session_start();
  }
  $_SESSION["loggedIn"] = true;
}

header("Location: ../administration.php");
exit();
