<?php

class Auto {

  private $id;
  private $Znacka;
  private $Model;
  private $Karoserie;
  private $Spotreba;
  private $Vybava;
  private $Palivo;
  private $PocetMist;
  private $Prevodovka;
  private $Cena;
  private $Img;

  public function __construct(array $data) {

    $this->id = $data['id'];
    $this->Znacka = $data['znacka'];
    $this->Model = $data['model'];
    $this->Karoserie = $data['karoserie'];
    $this->Spotreba = $data['spotreba'];
    $this->Vybava = explode(",", $data['vybava']);
    $this->Palivo = $data['palivo'];
    $this->PocetMist = $data['pocetMist'];
    $this->Prevodovka = $data['prevodovka'];
    $this->Cena = $data['cena'];
    $this->Img = $data['img'];
  }

  public function getId() {
    return $this->id;
  }

  public function getZnacka() {
    return $this->Znacka;
  }

  public function getModel() {
    return $this->Model;
  }

  public function getKaroserie() {
    return $this->Karoserie;
  }

  public function getSpotreba() {
    return $this->Spotreba;
  }

  public function getVybava() {
    return $this->Vybava;
  }
  public function setVybava($newVybava) {
    $this->Vybava = $newVybava;
  }

  public function getPalivo() {
    return $this->Palivo;
  }

  public function getPocetMist() {
    return $this->PocetMist;
  }

  public function getPrevodovka() {
    return $this->Prevodovka;
  }

  public function getCena() {
    return $this->Cena;
  }

  public function getImg() {
    return $this->Img;
  }

  public function toArray() {

    return
    [
        'id'   => $this->getId(),
        'znacka' => $this->getZnacka(),
        'model' => $this->getModel(),
        'karoserie' => $this->getKaroserie(),
        'spotreba' => $this->getSpotreba(),
        'vybava' => $this->getVybava(),
        'palivo' => $this->getPalivo(),
        'pocetMist' => $this->getPocetMist(),
        'prevodovka' => $this->getPrevodovka(),
        'cena' => $this->getCena(),
        'img' => $this->getImg()
    ];
}



}
