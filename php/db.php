<?php
include 'auto.php';

class DB {

  private $servername;
  private $username;
  private $password;
  private $dbname;

  public function connect() {

      $this->servername = "localhost";
      $this->username = "root";
      $this->password = "";
      $this->dbname = "fajn_pujcovna_aut";

      $conn = new mysqli (
        $this->servername,
        $this->username,
        $this->password,
        $this->dbname

      );

      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      } else {
        $conn->query("SET CHARACTER SET utf8");
        return $conn;
      }
    }

    public function getCars($sql = "SELECT * FROM auta ORDER BY id") {

        $auta = [];
        $conn = $this->connect();
        $result = $conn->query($sql);
        if (empty($result)) {return null;}
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
    
           $auto = new Auto($row);
           array_push($auta, $auto);
          }
        }
    
        $conn->close();
        return $auta;
    }

    public function reservation(
      $idAuta,
      $datum,
      $cena,
      $jmeno,
      $prijmeni,
      $email,
      $telefon,
      $obcanka,
      $ridicak,
      $stat,
      $obec,
      $ulice,
      $cisloPopisne,
      $psc
      ) 
    {
      $conn = $this->connect();
      $sql = "INSERT INTO rezervace (
        ID_auta,
        rezervovane_dny,
        cena,
        jmeno,
        prijmeni,
        email,
        telefon,
        obcanka,
        ridicak,
        stat,
        obec,
        ulice,
        cislo_popisne,
        psc
      ) VALUES (
        '$idAuta',
        '$datum',
        '$cena',
        '$jmeno',
        '$prijmeni',
        '$email',
        '$telefon',
        '$obcanka',
        '$ridicak',
        '$stat',
        '$obec',
        '$ulice',
        '$cisloPopisne',
        '$psc'
      )";

      if ($conn->query($sql) === TRUE) {
        $conn->close();
      } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        $conn->close();
      }
    }

    public function getReservations() {

      $rezervace = [];
      $conn = $this->connect();
      $sql = "SELECT * FROM rezervace ORDER BY id";
      $result = $conn->query($sql);
      if (empty($result)) {return null;}
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
  
         array_push($rezervace, $row);
        }
      }
  
      $conn->close();
      return $rezervace;
    }

  
  public function getAdminHash($username) {

    $conn = $this->connect();
    $sql = "SELECT password FROM admins WHERE username='$username' limit 1";
    $result = $conn->query($sql);
    if (empty($result)) {
        return null;
    } else {
        return $result->fetch_assoc()['password'];
    }

    $conn->close();
    return $usernames;
  }

  public function getAdmins() {

    $users = [];
    $conn = $this->connect();
    $sql = "SELECT id, username FROM admins ORDER BY id";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {

       array_push($users, $row);
      }
    }

    $conn->close();
    return $users;
  }

  public function updateCar($auto) {

    $conn = $this->connect();
    $auto['img'] = $this->insertSlashes($auto['img']);
    $sql = "UPDATE auta SET
      znacka='".$auto['znacka']."',
      model='".$auto['model']."',
      karoserie='".$auto['karoserie']."',
      spotreba='".$auto['spotreba']."',
      vybava='".$auto['vybava']."',
      palivo='".$auto['palivo']."',
      pocetMist='".$auto['pocetMist']."',
      prevodovka='".$auto['prevodovka']."',
      cena='".$auto['cena']."',
      img='".$auto['img']."'
    WHERE id=".$auto['id'];

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function insertSlashes($imgUrl) {
    $substr = '\\';
    $attachment = '\\';
    $newstring = str_replace($substr, $substr.$attachment, $imgUrl);

    return $newstring;
  }

  public function insertCar($auto) {
    $auto['img'] = $this->insertSlashes($auto['img']);
    $conn = $this->connect();
    $sql = "INSERT INTO auta (
      znacka,
      model,
      karoserie,
      spotreba,
      vybava,
      palivo,
      pocetMist,
      prevodovka,
      cena,
      img
    ) VALUES (
      '".$auto['znacka']."',
      '".$auto['model']."',
      '".$auto['karoserie']."',
      '".$auto['spotreba']."',
      '".$auto['vybava']."',
      '".$auto['palivo']."',
      '".$auto['pocetMist']."',
      '".$auto['prevodovka']."',
      '".$auto['cena']."',
      '".$auto['img']."'
    )";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function deleteCar($id) {

    $conn = $this->connect();
    $sql = "DELETE FROM auta WHERE id=".$id;
    if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }

    $conn->close();
  }

  public function updateReservation($rezervace) {

    $conn = $this->connect();
    $sql = "UPDATE rezervace SET
      ID_auta='".$rezervace['ID_auta']."',
      rezervovane_dny='".$rezervace['rezervovane_dny']."',
      cena='".$rezervace['cena']."',
      jmeno='".$rezervace['jmeno']."',
      prijmeni='".$rezervace['prijmeni']."',
      email='".$rezervace['email']."',
      telefon='".$rezervace['telefon']."',
      obcanka='".$rezervace['obcanka']."',
      ridicak='".$rezervace['ridicak']."',
      stat='".$rezervace['stat']."',
      obec='".$rezervace['obec']."',
      ulice='".$rezervace['ulice']."',
      cislo_popisne='".$rezervace['cislo_popisne']."',
      psc='".$rezervace['psc']."'
    WHERE id=".$rezervace['id'];

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function insertReservation($rezervace) {

    $conn = $this->connect();
    $sql = "INSERT INTO rezervace (
      ID_auta,
      rezervovane_dny,
      cena,
      jmeno,
      prijmeni,
      email,
      telefon,
      obcanka,
      ridicak,
      stat,
      obec,
      ulice,
      cislo_popisne,
      psc
    ) VALUES (
      '".$rezervace['ID_auta']."',
      '".$rezervace['rezervovane_dny']."',
      '".$rezervace['cena']."',
      '".$rezervace['jmeno']."',
      '".$rezervace['prijmeni']."',
      '".$rezervace['email']."',
      '".$rezervace['telefon']."',
      '".$rezervace['obcanka']."',
      '".$rezervace['ridicak']."',
      '".$rezervace['stat']."',
      '".$rezervace['obec']."',
      '".$rezervace['ulice']."',
      '".$rezervace['cislo_popisne']."',
      '".$rezervace['psc']."'
    )";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function deleteReservation($id) {

    $conn = $this->connect();
    $sql = "DELETE FROM rezervace WHERE id=".$id;
    if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }

    $conn->close();
  }

  public function updateAdmin($admin) {

    $conn = $this->connect();
    $sql = "UPDATE admins SET
      username='".$admin['username']."'
    WHERE id=".$admin['id'];

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function insertAdmin($admin) {

    $conn = $this->connect();
    $sql = "INSERT INTO admins (
      username,
      password
    ) VALUES (
      '".$admin['username']."',
      '".$admin['password']."'
    )";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
  }

  public function deleteAdmin($id) {

    $conn = $this->connect();
    $sql = "DELETE FROM admins WHERE id=".$id;
    if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }

    $conn->close();
  }
}