<?php
require 'db.php';
require '../libs/Smarty.class.php';

$smarty = new Smarty;
$db = new DB;
$auta = $db->getCars();

$AjaxFiltery = [];

$AjaxFilterArray = json_decode($_GET["filters"]);

$AjaxFiltery['znacka'] = $AjaxFilterArray->znacka;
$AjaxFiltery['karoserie'] = $AjaxFilterArray->karoserie;
$AjaxFiltery['palivo'] = $AjaxFilterArray->palivo;
$AjaxFiltery['prevodovka'] = $AjaxFilterArray->prevodovka;

switch($AjaxFiltery['znacka']) {
    case 0: $znacka = null; break;
    case 1: $znacka = "Audi"; break;
    case 2: $znacka = "BMW"; break;
    case 3: $znacka = "Ferrari"; break;
    case 4: $znacka = "Ford"; break;
    case 5: $znacka = "Hyundai"; break;
    case 6: $znacka = "Chevrolet"; break;
    case 7: $znacka = "Lamborghini"; break;
    case 8: $znacka = "Mazda"; break;
    case 9: $znacka = "Škoda"; break;
    case 10: $znacka = "Volkswagen"; break;
}

switch($AjaxFiltery['karoserie']) {
    case 0: $karoserie = null; break;
    case 1: $karoserie = "SUV"; break;
    case 2: $karoserie = "Kupé"; break;
    case 3: $karoserie = "Hatchback"; break;
    case 4: $karoserie = "Sedan"; break;
    case 5: $karoserie = "Liftback"; break;
    case 6: $karoserie = "Kombi"; break;
}

switch($AjaxFiltery['palivo']) {
    case 0: $palivo = null; break;
    case 1: $palivo = "Benzín"; break;
    case 2: $palivo = "Nafta"; break;
    case 3: $palivo = "Hybridní"; break;
}

switch($AjaxFiltery['prevodovka']) {
    case 0: $prevodovka = null; break;
    case 1: $prevodovka = "Manual"; break;
    case 2: $prevodovka = "Automat"; break;
}


if(!is_null($znacka)) {
    $auta = array_filter($auta, function($auto) use($znacka) {
        return $auto->getZnacka() == $znacka;
    });
}

if(!is_null($karoserie)) {
    $auta = array_filter($auta, function($auto) use($karoserie) {
        return $auto->getKaroserie() == $karoserie;
    });
}

if(!is_null($palivo)) {
    $auta = array_filter($auta, function($auto) use($palivo) {
        return $auto->getPalivo() == $palivo;
    });
}

if(!is_null($prevodovka)) {
    $auta = array_filter($auta, function($auto) use($prevodovka) {
        return $auto->getPrevodovka() == $prevodovka;
    });
}

$autaJSON = array();
foreach ($auta as $auto) {
  array_push($autaJSON, $auto->toArray());
}

echo json_encode($autaJSON);

