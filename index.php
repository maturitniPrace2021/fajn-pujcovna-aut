<?php
require 'php/db.php';
require 'libs/Smarty.class.php';

$smarty = new Smarty;
$db = new DB;

$filtery = array();

$filtery['znacka'] = array(
    0 => 'Vyberte značku',
    1 => 'Audi',
    2 => 'BMW',
    3 => 'Ferrari',
    4 => 'Ford',
    5 => 'Hyundai',
    6 => 'Chevrolet',
    7 => 'Lamborghini',
    8 => 'Mazda',
    9 => 'Škoda',
    10 => 'Volkswagen'
);

$filtery['karoserie'] = array(
    0 => 'Vyberte karoserii',
    1 => 'SUV',
    2 => 'Kupé',
    3 => 'Hatchback',
    4 => 'Sedan',
    5 => 'Liftback',
    6 => 'Kombi'
);

$filtery['palivo'] = array(
    0 => 'Vyberte typ paliva',
    1 => 'Benzín',
    2 => 'Nafta',
    3 => "Hybridní"
);

$filtery['prevodovka'] = array(
    0 => 'Vyberte typ převodovky',
    1 => 'Manual',
    2 => 'Automat'
);

$auta = $db->getCars();
$smarty->assign('filtery', $filtery);
$smarty->assign("auta", $auta);
$smarty->display('home.tpl');
