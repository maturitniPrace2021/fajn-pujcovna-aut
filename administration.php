<?php
require 'php/db.php';
require 'libs/Smarty.class.php';

$smarty = new Smarty;
$db = new DB;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {

  $auta = $db->getCars();
  
  foreach ($auta as $auto) {
    $vybavaString = "";
    foreach($auto->getVybava() as $vybava) {
      $vybavaString .= $vybava . ",";
    }
    $vybavaString = rtrim($vybavaString, ",");
    $auto->setVybava($vybavaString);
  }
  
  $rezervace = $db->getReservations();
  $admins = $db->getAdmins();
  $smarty->assign('auta', $auta);
  $smarty->assign('rezervace', $rezervace);
  $smarty->assign('admins', $admins);
  $smarty->display('administration.tpl');
} else {
  $smarty->display('adminLogin.tpl');
}
