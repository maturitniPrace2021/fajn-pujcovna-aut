<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style/main/main.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="icon" href="img/logo_male.png">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script defer src="js/toggleNav.js"></script>
	<title>Přihlášení do administrace</title>
</head>
<body class="loginBody">

{include 'header.tpl'}
  

  <div class="loginContainer">
	<div class="loginNadpis">
		<h2>Login</h2>
	</div>
	

	<form class="loginForm" action="php/login.php" method="post">
		<div>
			<label>Username</label>
			<input type="text" name="username">
		</div>
		<div>
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<span>
			<button type="submit">Login</button>
		</span>
	</form>
  </div>

</body>
</html>
