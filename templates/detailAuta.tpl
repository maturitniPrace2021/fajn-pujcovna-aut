<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="icon" href="img/logo_male.png">
    <link rel="stylesheet" href="style/main/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/detailAuta.js"></script>
    <script defer src="js/calendar.js"></script>
    <script defer src="js/toggleNav.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <title>Fajn-půjčovna aut</title>
  </head>
  <body>

  {include 'header.tpl'}
  {if ({$mailError} == 1)}
    <div class="mailFeedback mailFail">Rezervace se nezdařila. Vyberte den a zkuste to znovu.</div>
  {elseif ({$mailSuccess} == 1)}
    <div class="mailFeedback mailSuccess">Rezervace byla úspěšná. Na email Vám bylo odesláno potvrzení.</div>
  {/if}

  <div class="container">

    <div class="imgInfo">
      <div class="imgContainer">
        <img src="{$auto->getImg()}" alt="{$auto->getZnacka()}"/>
      </div>
      
      <div class="informace">
        <div class="row">
          <span class="bold">Značka:</span>
          <span>{$auto->getZnacka()}</span>
        </div>
        <div class="row">
          <span class="bold">Model:</span>
          <span>{$auto->getModel()}</span>
        </div>
        <div class="row">
          <span class="bold">Karosérie:</span>
          <span>{$auto->getKaroserie()}</span>
        </div>
        <div class="row">
          <span class="bold">Spotřeba:</span>  
          <span>{$auto->getSpotreba()}  l/100 km</span>
        </div>
        <div class="row">
          <span class="bold">Výbava:</span>
          <i id="vybavaSipka" class='fa fa-arrow-circle-o-down' onclick="vybavaToggle();"></i>
          <div id="informaceVybava">
            {foreach from=$auto->getVybava() item=jednaVybava}
              <span>-{$jednaVybava}</span>
            {/foreach}
          </div>
        </div>
        <div class="row">
          <span class="bold">Palivo:</span>
          <span>{$auto->getPalivo()}</span>
        </div>
        <div class="row">
          <span class="bold">Počet míst:</span>
          <span>{$auto->getPocetMist()}</span>
        </div>
        <div class="row">
          <span class="bold">Převodovka:</span>
          <span>{$auto->getPrevodovka()}</span>
        </div>
        <div class="row">
          <span class="bold">Cena:</span>
          <span>{$auto->getCena()} Kč</span>
        </div>
      </div>
    </div>

    <div class="formularKalendar">

      <div class="calendar">
        <div class="month">
          <ul>
            <li class="prev fa fa-arrow-left" onclick="prevMonth()"></li>
            <li id="monthAndYear"></li>
            <li class="next fa fa-arrow-right" onclick="nextMonth()"></li>
          </ul>
        </div>
        <ul id="weekDays"></ul>
        <ul id="monthDays"></ul>
        <div class="calendarFooter">
          <div class="footerRow">
            <div class="vybraneDnyHeader">Vybrané dny: </div>
            <div id="vybraneDny">Zatím jste nevybrali žádné datum.</div>
          </div>
          <div class="footerRow">
            <div class="cenaHeader">Cena: </div>
            <div id="cena">0 Kč</div>
          </div>
        </div>
      </div>

      <div class="formular">
        <form action="php/rezervace.php" method="post">
          <input type="hidden" name="idAuta" value="{$auto->getId()}">
          <input name="jmeno" placeholder="Jméno ..." required>
          <input name="prijmeni" placeholder="Příjmení ..." required>
          <input name="email" type="email" placeholder="Email (xxx@xxx.xx)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{literal}{2,}{/literal}$" required>
          <input name="telefon" type="tel" placeholder="Telefonní číslo ..." required>
          <input name="obcanka" placeholder="Číslo občanského průkazu ..." required>
          <input name="ridicak" placeholder="Číslo řidičského průkazu ..." required>
          <input name="stat" placeholder="Stát ..." required>
          <input name="obec" placeholder="Obec ..." required>
          <input name="ulice" placeholder="Ulice ..." required>
          <input name="cisloPopisne" placeholder="Č.P." required>
          <input type="number" name="psc" placeholder="PSČ ..." required>
          <input type="submit" value="Odeslat" class="button">
          <input type="hidden" name="cena" value="">
          <input type="hidden" name="vybraneDny" value="">
          <input type="hidden" name="nazevAuta" value="{$auto->getZnacka()} {$auto->getModel()}">
          
        </form>
      </div>
      
      <span id="zabraneDny" style="display:none;">{$zabraneDny}</span>
      <input type="hidden" name="cenaAuta" value="{$auto->getCena()}">

    </div>



  </div>

  </body>
</html>
