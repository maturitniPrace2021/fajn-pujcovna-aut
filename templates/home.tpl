<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="img/logo_male.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style/main/main.css">
    <script src="js/ajax.js"></script>
    <script defer src="js/toggleNav.js"></script>
    <title>Fajn-půjčovna aut</title>
  </head>
  <body id="home">

  {include 'header.tpl'}
  
  <div class="content">

    <div class="filtery">
      <select id="znacka" class="textarea" onchange="showCars()">
        {foreach from=$filtery['znacka'] key=id item=txt}
          <option value={$id}>{$txt}</option>
        {/foreach}
      </select>
  
      <select id="karoserie" class="textarea" onchange="showCars()">
        {foreach from=$filtery['karoserie'] key=id item=txt}
          <option value={$id}>{$txt}</option>
        {/foreach}
      </select>
  
      <select id="palivo" class="textarea" onchange="showCars()">
        {foreach from=$filtery['palivo'] key=id item=txt}
          <option value={$id}>{$txt}</option>
        {/foreach}
      </select>
  
      <select id="prevodovka" class="textarea" onchange="showCars()">
        {foreach from=$filtery['prevodovka'] key=id item=txt}
          <option value={$id}>{$txt}</option>
        {/foreach}
      </select>
    </div>

    <div id="wrapper">
      {foreach from=$auta item=auto}
        <div>
          <a href="detailAuta.php?id={$auto->getId()}">
            <img src="{$auto->getImg()}" alt="{$auto->getModel()}">
            <div>{$auto->getZnacka()} {$auto->getModel()}</div>
          </a>
        </div>
      {/foreach}
    </div>

    <div id="cenik">

      <h1>Ceník osobních automobilů</h1> <br/>

      <p> <b>Kauce na osobní automobily činí 10 000 Kč.</b> <br/>

        Po navrácení auta v původním stavu bez poškození bude tato kauce <b>navrácena.</b> <br/>

        Uvedená cena je na <b>24 hodin.</b></p> <br/> 

    </div>

    <div id="kontakty">
    
    <h1>Kontakt</h1> <br/> 

        <p>Telefon: <b>605 367 982 </b> <br/> E-mail: <b> autopujcovna1@gmail.com </b>  </p> <br/>
    
    
    </div>

    <div id="kontakt">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d82507.32058743977!2d13.301883882223812!3d49.74178702397769!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470af1e5133d11b7%3A0x31b9406e3fc10b83!2zUGx6ZcWI!5e0!3m2!1scs!2scz!4v1611082662070!5m2!1scs!2scz" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    
  </div>

  </body>
</html>
