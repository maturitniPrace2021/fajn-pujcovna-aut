<!DOCTYPE html>
<html lang="cs">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style/main/main.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="icon" href="img/logo_male.png">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <script src="js/administration.js"></script>
    <title>Administrace</title>
</head>

<body>

    <nav class="adminNav">
        <ul>
            <div>
                <li onclick="displayForm(1);">Auta</li>
                <li onclick="displayForm(2);">Rezervace</li>
                <li onclick="displayForm(3);">Admins</li>
            </div>
            <li><a href="php/odhlaseni.php">Odhlásit</a></li>
        </ul>
    </nav>

    <form id="formAuta" action="php/updateDatabase.php" method="post">
        <div class="adminUdaje">
            <table id="tableAuta">
                <tr>
                <th>Smazat</th>
                <th>id</th>
                <th>znacka</th>
                <th>model</th>
                <th>karoserie</th>
                <th>spotreba</th>
                <th>vybava</th>
                <th>palivo</th>
                <th>pocetMist</th>
                <th>prevodovka</th>
                <th>cena</th>
                <th>img</th>
                </tr>
            
                {foreach from=$auta item=auto}
                <tr>
                <td><input type="checkbox" name="delete[]" value="{$auto->getId()}"></td>
                <td><input type="text" name="id[]" value="{$auto->getId()}" readonly></td>
                <td><input type="text" name="znacka[]" value="{$auto->getZnacka()}" required></td>
                <td><input type="text" name="model[]" value="{$auto->getModel()}" required></td>
                <td><input type="text" name="karoserie[]" value="{$auto->getKaroserie()}" ></td>
                <td><input type="text" name="spotreba[]" value="{$auto->getSpotreba()}" ></td>
                <td><input type="text" name="vybava[]" value="{$auto->getVybava()}" ></td>
                <td><input type="text" name="palivo[]" value="{$auto->getPalivo()}" ></td>
                <td><input type="text" name="pocetMist[]" value="{$auto->getPocetMist()}" ></td>
                <td><input type="text" name="prevodovka[]" value="{$auto->getPrevodovka()}" ></td>
                <td><input type="text" name="cena[]" value="{$auto->getCena()}" ></td>
                <td><input type="text" name="img[]" value="{$auto->getImg()}" ></td>
                </tr>
                {/foreach}
                <input type="text" name="form" value="auta" style="display: none;">
            </table>
        </div>
        
        <input class="adminButton" type="submit">
        <div>
            <input class="adminButton" type="button" value="Přidat auto" onclick="addRowAuta()">
        </div>
    </form>

    <form id="formRezervace" action="php/updateDatabase.php" method="post">
        <div class="adminUdaje">
            <table id="tableRezervace">
                <tr>
                    <th>Smazat</th>
                    <th>id</th>
                    <th>ID auta</th>
                    <th>Rezervovane dny</th>
                    <th>Cena</th>
                    <th>Jmeno</th>
                    <th>Prijmeni</th>
                    <th>Email</th>
                    <th>Telefon</th>
                    <th>Obcanka</th>
                    <th>Ridicak</th>
                    <th>Stat</th>
                    <th>Obec</th>
                    <th>Ulice</th>
                    <th>Cislo popisne</th>
                    <th>PSC</th>
                </tr>

                {foreach from=$rezervace item=jednaRezervace}
                <tr>
                    <td><input type="checkbox" name="delete[]" value="{$jednaRezervace.id}"></td>
                    <td><input type="text" name="id[]" value="{$jednaRezervace.id}" readonly></td>
                    <td><input type="number" name="ID_auta[]" value="{$jednaRezervace.ID_auta}"></td>
                    <td><input type="text" name="rezervovane_dny[]" value="{$jednaRezervace.rezervovane_dny}"></td>
                    <td><input type="number" name="cena[]" value="{$jednaRezervace.cena}"></td>
                    <td><input type="text" name="jmeno[]" value="{$jednaRezervace.jmeno}"></td>
                    <td><input type="text" name="prijmeni[]" value="{$jednaRezervace.prijmeni}"></td>
                    <td><input type="text" name="email[]" value="{$jednaRezervace.email}"></td>
                    <td><input type="text" name="telefon[]" value="{$jednaRezervace.telefon}"></td>
                    <td><input type="text" name="obcanka[]" value="{$jednaRezervace.obcanka}"></td>
                    <td><input type="text" name="ridicak[]" value="{$jednaRezervace.ridicak}"></td>
                    <td><input type="text" name="stat[]" value="{$jednaRezervace.stat}"></td>
                    <td><input type="text" name="obec[]" value="{$jednaRezervace.obec}"></td>
                    <td><input type="text" name="ulice[]" value="{$jednaRezervace.ulice}"></td>
                    <td><input type="text" name="cislo_popisne[]" value="{$jednaRezervace.cislo_popisne}"></td>
                    <td><input type="number" name="psc[]" value="{$jednaRezervace.psc}"></td>
                </tr>
                {/foreach}
                <input type="text" name="form" value="rezervace" style="display:none;">

            </table>
        </div>
        <input class="adminButton" type="submit">
        <div>
            <input class="adminButton" type="button" value="Přidat rezervaci" onclick="addRowRezervace()">
        </div>
    </form>

    <form id="formAdmins" action="php/updateDatabase.php" method="post">
        <div class="adminUdaje">
            <table id="tableAdmins">
                <tr>
                <th>Smazat</th>
                <th>id</th>
                <th>Username</th>
                <th>Password</th>
                </tr>
            
                {foreach from=$admins item=admin}
                <tr>
                <td><input type="checkbox" name="delete[]" value="{$admin.id}"></td>
                <td><input type="text" name="id[]" value="{$admin.id}" readonly></td>
                <td><input type="text" name="username[]" value="{$admin.username}"></td>
                <td><input type="text" name="password[]" value="" placeholder="Zahashovane heslo"></td>
                </tr>
                {/foreach}
                <input type="text" name="form" value="admins" style="display: none;">
            
            </table>
        </div>
        
          <input class="adminButton" type="submit">
          <div><input class="adminButton" type="button" value="Přidat uživatele" onclick="addRowAdmins()"></div>
    </form>


</body>

</html>