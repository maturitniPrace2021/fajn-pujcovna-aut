
var vybavaOpened = false;
function vybavaToggle() {
    
    if(vybavaOpened == false) {
        document.getElementById("informaceVybava").style = "display: block;";
        document.getElementById("vybavaSipka").style = "transform: rotate(180deg);";
        vybavaOpened = true; 
    } else {
        document.getElementById("informaceVybava").style = "display: none;";
        document.getElementById("vybavaSipka").style = "transform: rotate(0deg);";
        vybavaOpened = false;
    }
    
}