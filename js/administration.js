function displayForm(form) {
  switch(form) {
    case 1:
      document.getElementById("formAuta").style = "display: block;"
      document.getElementById("formRezervace").style = "display: none;"
      document.getElementById("formAdmins").style = "display: none;"
      break;
    case 2:
      document.getElementById("formAuta").style = "display: none;"
      document.getElementById("formRezervace").style = "display: block;"
      document.getElementById("formAdmins").style = "display: none;"
      break;
    case 3:
      document.getElementById("formAuta").style = "display: none;"
      document.getElementById("formRezervace").style = "display: none;"
      document.getElementById("formAdmins").style = "display: block;"
      break;
  }
}
function addRowRezervace() {
    var tableContent = document.getElementById("tableRezervace").innerHTML;
    var text = "";

    text += "<tr>";
    text += "<td></td>";
    text += "<td><input type='text' name='id[]' value='automaticky' readonly></td>";
    text += "<td><input type='number' name='ID_auta[]' value=''></td>";
    text += "<td><input type='text' name='rezervovane_dny[]' value=''></td>";
    text += "<td><input type='number' name='cena[]' value=''></td>";
    text += "<td><input type='text' name='jmeno[]' value=''></td>";
    text += "<td><input type='text' name='prijmeni[]' value=''></td>";
    text += "<td><input type='text' name='email[]' value=''></td>";
    text += "<td><input type='text' name='telefon[]' value=''></td>";
    text += "<td><input type='text' name='obcanka[]' value=''></td>";
    text += "<td><input type='text' name='ridicak[]' value=''></td>";
    text += "<td><input type='text' name='stat[]' value=''></td>";
    text += "<td><input type='text' name='obec[]' value=''></td>";
    text += "<td><input type='text' name='ulice[]' value=''></td>";
    text += "<td><input type='text' name='cislo_popisne[]' value=''></td>";
    text += "<td><input type='number' name='psc[]' value=''></td>";
    text += "</tr>";

    tableContent = tableContent.replace("</tbody>", "");
    tableContent += text;
    tableContent += "</tbody>";
    document.getElementById("tableRezervace").innerHTML = tableContent;
  }
  function addRowAuta() {
    var tableContent = document.getElementById("tableAuta").innerHTML;
    var text = "";

    text += "<tr>";
    text += "<td></td>";
		text += "<td><input type='text' name='id[]' value='automaticky' readonly></td>";
		text += "<td><input type='text' name='znacka[]' value=''></td>";
		text += "<td><input type='text' name='model[]' value=''></td>";
		text += "<td><input type='text' name='karoserie[]' value=''></td>";
		text += "<td><input type='text' name='spotreba[]' value=''></td>";
		text += "<td><input type='text' name='vybava[]' value=''></td>";
		text += "<td><input type='text' name='palivo[]' value=''></td>";
		text += "<td><input type='text' name='pocetMist[]' value=''></td>";
		text += "<td><input type='text' name='prevodovka[]' value=''></td>";
		text += "<td><input type='text' name='cena[]' value=''></td>";
		text += "<td><input type='text' name='img[]' value='' ></td>";
		text += "</tr>";

    tableContent = tableContent.replace("</tbody>", "");
    tableContent += text;
    tableContent += "</tbody>";
    document.getElementById("tableAuta").innerHTML = tableContent;
  }
  var adminRows = 0;
  function addRowAdmins() {
    var tableContent = document.getElementById("tableAdmins").innerHTML;
    var text = "";
    adminRows++;

    text += "<tr>";
    text += "<td></td>";
    text += "<td><input type='text' name='id[]' value='automaticky' readonly></td>";
    text += "<td><input type='text' name='username[]' value=''></td>";
    text += "<td><input id='togglePassword-"+adminRows+"' type='password' name='password[]' value=''><input class='togglePassword' type='checkbox' onclick='toggleShowPassword("+adminRows+")'>Show</td>";
    text += "</tr>";

    tableContent = tableContent.replace("</tbody>", "");
    tableContent += text;
    tableContent += "</tbody>";
    document.getElementById("tableAdmins").innerHTML = tableContent;
  }
  function toggleShowPassword(num) {
    let toggleId = "togglePassword-" + num;
    var toggle = document.getElementById(toggleId);
    if (toggle.type === "password") {
      toggle.type = "text";
    } else {
      toggle.type = "password";
    }
  }
