
var date = new Date();

const months = [
  "leden",
  "únor",
  "březen",
  "duben",
  "květen",
  "červen",
  "červenec",
  "srpen",
  "září",
  "říjen",
  "listopad",
  "prosinec"
  ];

  const days = [
    "Ne",
    "Po",
    "Út",
    "St",
    "Čt",
    "Pá",
    "So"
  ];

var thisMonth = date.getMonth()+1;
var thisMonthName = months[thisMonth-1];
var thisDate = date.getDate();
var thisYear = date.getFullYear();
var selectedDays = [];

function getWeekDaysText() {

  var text = "";
  for(var i = 1; i<days.length; i++) {
    text += "<li>" + days[i] + "</li>";
  }
  text += "<li>" + days[0] + "</li>";

  return text;
}

function getMonthDays(month, year) {

  return new Date(year, month, 0).getDate();
}

function getMonthDaysText() {

  var monthDays = getMonthDays(thisMonth, thisYear);
  var firstDay = firstDayInMonth(thisMonth, thisYear);
  var text = "";

  for(var den = 1; den < firstDay; den++) {
    text += "<li></li>";
  }

  for(var den = 1; den < monthDays+1; den++) {
    if(dateIsPossible(den)) {
      if(dateIsBooked(den)) {
        text += "<li class='booked'><span class='booked'>" + den + "</span></li>";
      } else {
        text += "<li id='"+formateDate(den, thisMonth)+"' onclick='selectDay("+den+")'>" + den + "</li>";
      }
    } else {
      text += "<li class='inactive'><span>" + den + "</span></li>";
    }
  }

  return text;
}

function dateIsPossible(d) {

  if (date.getFullYear() == thisYear && date.getMonth()+1 == thisMonth && date.getDate() >= d) {
      return false;
  } else if(date.getFullYear() == thisYear && date.getMonth()+1 == thisMonth && date.getDate() < d) {
      return true;
  } else if(date.getFullYear() == thisYear && date.getMonth()+1 > thisMonth) {
      return false;
  } else if(date.getFullYear() == thisYear && date.getMonth()+1 < thisMonth) {
      return true;
  } else if (date.getFullYear() >= thisYear) {
      return false;
  } else if (date.getFullYear() <= thisYear) {
      return true;
  }
}

function dateIsBooked(d) {
  var zabraneDnyArray = document.getElementById("zabraneDny").innerHTML.split(",");
  for(var i = 0; i < zabraneDnyArray.length; i++) {
    if(zabraneDnyArray[i] == formateDate(d, thisMonth)) {
      return true;
    }
  }
  return false;
}

function nextMonth() {

  if(thisMonth == 12) {
    thisMonth = 1;
    thisYear += 1;
  } else {
    thisMonth += 1;
  }

  thisMonthName = months[thisMonth-1];
  writeText();
}

function prevMonth() {

  if(thisMonth == 1) {
    thisMonth = 12;
    thisYear -= 1;
  } else {
    thisMonth -= 1;
  }

  thisMonthName = months[thisMonth-1];
  writeText();
}

function firstDayInMonth(month, year) {

  if(month == 1) {
    var monthBefore = 12;
    var yearBefore = thisYear-1;
    var datum = new Date(yearBefore, monthBefore, 0);
  } else {
    var monthBefore = month - 1;
    var datum = new Date(year, monthBefore, 0);
  }

  var firstDay = datum.getDay()+1;

  return firstDay;
}

function selectDay(day) {

  var selectedDay = formateDate(day, thisMonth);
  selectedDays.push(selectedDay);
  document.getElementById(selectedDay).style.color = "#32a852"; //green
  for(var i = 0; i < selectedDays.length-1; i++) {
    if(selectedDays[i] == selectedDay) {
      selectedDays.pop();
      document.getElementById(selectedDay).style.color = "black";
      selectedDays.splice(i, 1);
      break;
    }
  }

  var vybraneDnyText = "";
  for(var i = 0; i<selectedDays.length; i++) {
    vybraneDnyText += "<span>" + selectedDays[i] + "</span> "
  }

  if(vybraneDnyText == "") {
    vybraneDnyText = "Zatím jste nevybrali žádné datum."
  }

  var cena = 0;
  const cenaAuta = document.getElementsByName("cenaAuta")[0].value;
  for(var i = 0; i<selectedDays.length; i++) {
      cena += parseInt(cenaAuta);
  }

  document.getElementsByName("vybraneDny")[0].value = selectedDays;
  document.getElementById("cena").innerHTML = cena + " Kč";
  document.getElementsByName("cena")[0].value = cena;
  document.getElementById("vybraneDny").innerHTML = vybraneDnyText;
}



function formateDate(day, month) {
  text = "";
  if (day < 10) {
		day = '0' + day;
	}

  if (month < 10) {
		month = '0' + month;
  }
  text += day + "." + month + "." + thisYear;

  return text;
}

function writeText() {

  document.getElementById("monthAndYear").innerHTML = thisMonthName + "<br>" + thisYear;
  document.getElementById("monthDays").innerHTML = getMonthDaysText();

  for(var i = 0; i < selectedDays.length; i++) {

    var elementExists = document.getElementById(selectedDays[i]);
    if(elementExists) {
      document.getElementById(selectedDays[i]).style.color = "#f07800";
    }
  }
}

writeText();
document.getElementById("weekDays").innerHTML = getWeekDaysText();
