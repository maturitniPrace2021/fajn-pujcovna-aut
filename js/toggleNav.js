const nav = document.getElementById("nav");
var navOpened = false;

function toggleNav() {
    if(navOpened) {
        nav.style = "display: none;";
    } else {
        nav.style = "display: flex;";
    }
    navOpened = !navOpened;
}