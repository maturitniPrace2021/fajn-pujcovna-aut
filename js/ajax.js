function showCars() {
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    
  var autaArray = JSON.parse(napraveniPasekyPoReklameNaHostingu(this.response));
  
  var text = "";
      for (i = 0; i < autaArray.length; i++) {

    text += "<div class='item'>";        
    
    text += "<a href='detailAuta.php?id="+autaArray[i]['id']+"'>";
    text += "<img src='" + autaArray[i]['img'] + "' alt='" + autaArray[i]['model'] + "'></img>";
    text += "<div>" + autaArray[i]['znacka'] + " " + autaArray[i]['model'] + "</div>";
    text += "</a>";
    
    text += "</div>";
  }

      
  if(autaArray.length == 0) {
      text += "<h2>Nebyla nalezena žádná auta odpovídající filterům</h2>";
  }

      
  document.getElementById("wrapper").innerHTML = text;
}
}

var selects = getSelects();

xhttp.open("GET", "php/ajaxFilter.php?filters="+JSON.stringify(selects), true);
xhttp.send();
  }

  function getSelects() {

  var znacka = document.getElementById("znacka");
  var karoserie = document.getElementById("karoserie");
  var palivo = document.getElementById("palivo");
  var prevodovka = document.getElementById("prevodovka");

  var selects = {
      znacka: znacka.options[znacka.selectedIndex].value,
      karoserie: karoserie.options[karoserie.selectedIndex].value,
      palivo: palivo.options[palivo.selectedIndex].value,
      prevodovka: prevodovka.options[prevodovka.selectedIndex].value

  }

  return selects;
  }
  
  function napraveniPasekyPoReklameNaHostingu(input) {
      let index = input.indexOf("[");
      return input.slice(index);
  }